
S = BoolSemiring

@testset failfast=true "NGramFSTs" begin
    counts = countngrams([[1, 2, 3, 1, 2, 3], [1, 2, 3]])

    @test counts[(0, 0, 1)] == 2
    @test counts[(0, 1, 2)] == 2
    @test counts[(1, 2, 3)] == 3
    @test counts[(2, 3, 1)] == 1
    @test counts[(3, 1, 2)] == 1
    @test counts[(2, 3, 1)] == 1

    lm = ngramfst(w -> one(S), counts)

    A = TensorFST(
        [
            (src = (1,), isym = 1, osym = 1, weight = one(S), dest = (2,)),
            (src = (2,), isym = 2, osym = 2, weight = one(S), dest = (3,)),
            (src = (3,), isym = 3, osym = 3, weight = one(S), dest = (4,)),
            (src = (4,), isym = 1, osym = 1, weight = one(S), dest = (5,)),
            (src = (5,), isym = 2, osym = 2, weight = one(S), dest = (3,))
        ],
        [(1,) => one(S)],
        [(4,) => one(S)]
    )
    @test issame(A, lm)

end
