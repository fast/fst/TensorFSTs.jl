
using Semirings
using TensorFSTs
using Test

using TensorFSTs.LinearFSTs
#using TensorFSTs.NGramFSTs
#using TensorFSTs.SpeechRecognitionFSTs

# @testset "SymbolTable" begin
#     symtable = SymbolTable(["a", "b"])
#     @test length(symtable) == 3
#     @test cmp(symtable[1], "ϵ") == 0
#     @test cmp(symtable[2], "a") == 0
#     @test cmp(symtable[3], "b") == 0
#     @test symtable["ϵ"] == 1
#     @test symtable["a"] == 2
#     @test symtable["b"] == 3

#     @test all(s -> s ∈ ["a", "b"], symbols(symtable))
#     @test all(s -> s ∈ ["ϵ", "a", "b"], symbols(symtable; exclude_ϵ = false))

#     @test all(i -> i ∈ [2, 3], ids(symtable))
#     @test all(i -> i ∈ [1, 2, 3], ids(symtable; exclude_ϵ = false))

#     symtable = SymbolTable()
#     @test length(symtable) == 1
# end


# Since we are using exact comparison with Set, we cannot test with float-based
# semiring types. Essentially, the test is mostly checking the topology of the
# FST.
function issame(A::SparseTensorFSM, B::SparseTensorFSM)
   Set(arcs(A)) == Set(arcs(B)) || return false
   Set(A.α) == Set(B.α) || return false
   Set(A.ω) == Set(B.ω) || return false   
end

#function isapprox(A::TensorFST, B::TensorFST)
#    arcs_A, arcs_B = sort(arcs(A)), sort(arcs(B))
#    for (a_a, a_b) in zip(arcs_A, arcs_B)
#
#        # src, isym, osym, dest
#        if !((a_a.src == a_b.src) && (a_a.isym == a_b.isym) && (a_a.osym == a_b.osym) && (a_a.dest == a_b.dest))
#            return false
#        end
#        # Weights
#        if !(val(a_a.weight) ≈ val(a_b.weight))
#            return false
#        end
#    end
#    Set(initweights(A)) == Set(initweights(B)) || return false
#    Set(finalweights(A)) == Set(finalweights(B)) || return false
#end


#@testset failfast=true "TensorFST" begin
#    S = BoolSemiring
#
#    all_arcs = [
#        (src=(1,), isym=2, osym=2, weight=one(S), dest=(1,)),
#        (src=(1,), isym=2, osym=2, weight=one(S), dest=(2,)),
#        (src=(2,), isym=2, osym=2, weight=one(S), dest=(2,)),
#		(src=(2,), isym=2, osym=2, weight=one(S), dest=(3,)),
#		(src=(3,), isym=2, osym=2, weight=one(S), dest=(3,))
#    ]
#    iweights = [(1,) => one(S)]
#    fweights = [(3,) => one(S)]
#    fst = TensorFST(all_arcs, iweights, fweights; Σrange = 1:3, Ωrange = 0:3)
#
#    @test numarcs(fst) == 5
#    @test semiring(fst) == S
#    @test Set(arcs(fst)) == Set(all_arcs)
#    @test Set(initweights(fst)) == Set(iweights)
#    @test Set(finalweights(fst)) == Set(fweights)
#    @test isymrange(fst) == 1:3
#    @test osymrange(fst) == 0:3
#    @test ! hasinputepsilon(fst)
#    @test hasoutputepsilon(fst)
#
#    A = TensorFST(
#        [
#           Arc(src=1, isym=2, osym=2, weight=one(S), dest=1),
#           Arc(src=1, isym=2, osym=2, weight=one(S), dest=2),
#           Arc(src=2, isym=2, osym=2, weight=one(S), dest=2),
#		   Arc(src=2, isym=2, osym=2, weight=one(S), dest=3),
#		   Arc(src=3, isym=2, osym=2, weight=one(S), dest=3)
#        ],
#        [1 => one(S)],
#        [3 => one(S)]
#    )
#
#    @test issame(A, fst)
#
#
#    A = TensorFST(
#        Any[
#           Arc(src=1, isym=2, osym=2, weight=one(S), dest=1),
#           Arc(src=1, isym=2, osym=2, weight=one(S), dest=2),
#           Arc(src=2, isym=2, osym=2, weight=one(S), dest=2),
#		   Arc(src=2, isym=2, osym=2, weight=one(S), dest=3),
#		   Arc(src=3, isym=2, osym=2, weight=one(S), dest=3)
#        ],
#        Any[1 => one(S)],
#        Any[3 => one(S)]
#    )
#
#    @test issame(A, fst)
#
#    fst2 = updatesymrange(fst, Σrange = -10:10, Ωrange = -10:10)
#    @test isymrange(fst2) == -10:10
#    @test osymrange(fst2) == -10:10
#end
#
#
#@testset verbose=true "Rational Operations" begin
#    S = BoolSemiring
#
#    A = TensorFST(
#        [
#            (src = (1,), isym = 2, osym = 2, weight = one(S), dest = (2,)),
#            (src = (2,), isym = 3, osym = 3, weight = one(S), dest = (3,)),
#            (src = (1,), isym = 2, osym = 3, weight = one(S), dest = (3,))
#        ],
#        [(1,) => one(S)],
#        [(3,) => one(S)]
#    )
#
#    B = TensorFST(
#        [
#            (src = (1,), isym = 2, osym = 2, weight = one(S), dest = (2,))
#        ],
#        [(1,) => one(S)],
#        [(2,) => one(S)];
#        Σrange = 1:3,
#        Ωrange = 1:3
#    )
#
#    @testset "union" begin
#        C = TensorFST(
#            [
#                (src = (1,), isym = 2, osym = 2, weight = one(S), dest = (2,)),
#                (src = (2,), isym = 3, osym = 3, weight = one(S), dest = (3,)),
#                (src = (1,), isym = 2, osym = 3, weight = one(S), dest = (3,)),
#                (src = (4,), isym = 2, osym = 2, weight = one(S), dest = (5,))
#            ],
#            [(1,) => one(S), (4,) => one(S)],
#            [(3,) => one(S), (5,) => one(S)]
#        )
#
#        @test issame(C, union(A, B))
#    end
#
#    @testset "concatenation" begin
#        C = TensorFST(
#            [
#                (src = (1,), isym = 2, osym = 2, weight = one(S), dest = (2,)),
#                (src = (1,), isym = 2, osym = 3, weight = one(S), dest = (3,)),
#                (src = (2,), isym = 3, osym = 3, weight = one(S), dest = (3,)),
#                (src = (3,), isym = 1, osym = 1, weight = one(S), dest = (4,)),
#                (src = (4,), isym = 2, osym = 2, weight = one(S), dest = (5,))
#            ],
#            [(1,) => one(S)],
#            [(5,) => one(S)]
#        )
#
#        @test issame(C, cat(A, B))
#    end
#
#    @testset "closure" begin
#        C = TensorFST(
#            [
#                (src = (3,), isym = 2, osym = 2, weight = one(S), dest = (4,)),
#                (src = (3,), isym = 2, osym = 3, weight = one(S), dest = (5,)),
#                (src = (4,), isym = 3, osym = 3, weight = one(S), dest = (5,)),
#                (src = (5,), isym = 1, osym = 1, weight = one(S), dest = (3,)),
#                (src = (1,), isym = 1, osym = 1, weight = one(S), dest = (2,))
#            ],
#            [(1,) => one(S), (3,) => one(S)],
#            [(2,) => one(S), (5,) => one(S)]
#        )
#
#        @test issame(C, closure(A))
#    end
#end
#
#
#@testset verbose=true "Shortestdistance" begin
#    S = ProbSemiring{Float64}
#
#    A = TensorFST(
#        [
#            (src = (1,), isym = 2, osym = 2, weight = S(2), dest = (2,)),
#            (src = (1,), isym = 3, osym = 2, weight = S(2), dest = (2,)),
#            (src = (2,), isym = 3, osym = 3, weight = S(5), dest = (4,)),
#            (src = (1,), isym = 2, osym = 3, weight = S(3), dest = (3,)),
#            (src = (3,), isym = 3, osym = 3, weight = S(2), dest = (4,))
#        ],
#        [(1,) => one(S)],
#        [(4,) => S(2)]
#    )
#
#    @test all(val.(shortestdistance(A)) .≈ [1, 4, 3, 26])
#    @test val(weight(A)) .≈ ((2+2)*5+(3*2))*2
#
#end
#
#
#@testset verbose=true "Composition" begin
#    S = BoolSemiring
#
#    A = TensorFST(
#        [
#            (src = (1,), isym = 2, osym = 3, weight = one(S), dest = (2,)),
#            (src = (2,), isym = 2, osym = 3, weight = one(S), dest = (1,)),
#            (src = (2,), isym = 3, osym = 3, weight = one(S), dest = (3,)),
#            (src = (2,), isym = 3, osym = 3, weight = one(S), dest = (4,)),
#            (src = (3,), isym = 2, osym = 3, weight = one(S), dest = (4,)),
#            (src = (4,), isym = 2, osym = 2, weight = one(S), dest = (4,))
#        ],
#        [(1,) => one(S)],
#        [(4,) => one(S)]
#    )
#
#    B = TensorFST(
#        [
#            (src = (1,), isym = 3, osym = 3, weight = one(S), dest = (2,)),
#            (src = (2,), isym = 3, osym = 2, weight = one(S), dest = (2,)),
#            (src = (2,), isym = 2, osym = 3, weight = one(S), dest = (3,)),
#            (src = (2,), isym = 2, osym = 3, weight = one(S), dest = (4,)),
#            (src = (3,), isym = 3, osym = 2, weight = one(S), dest = (4,))
#        ],
#        [(1,) => one(S)],
#        [(4,) => one(S)]
#    )
#
#    C = TensorFST(
#        [
#            (src = (1,), isym = 2, osym = 3, weight = one(S), dest = (3,)),
#            (src = (2,), isym = 2, osym = 2, weight = one(S), dest = (3,)),
#            (src = (3,), isym = 2, osym = 2, weight = one(S), dest = (2,)),
#            (src = (3,), isym = 3, osym = 2, weight = one(S), dest = (4,)),
#            (src = (3,), isym = 3, osym = 2, weight = one(S), dest = (5,)),
#            (src = (4,), isym = 2, osym = 2, weight = one(S), dest = (5,)),
#            (src = (5,), isym = 2, osym = 3, weight = one(S), dest = (6,))
#        ],
#        [(1,) => one(S)],
#        [(6,) => one(S)]
#    )
#    @test issame(C, compose(A, sort(B, inputlabel); connect = true))
#
#
#    syms = SymbolTable(["a", "b", "c", "d", "e"])
#    A = TensorFST(
#        [
#            (src = 1, isym = syms["a"], osym = syms["a"], weight = one(S), dest = 2),
#            (src = 2, isym = syms["b"], osym = syms["ϵ"], weight = one(S), dest = 3),
#            (src = 3, isym = syms["c"], osym = syms["ϵ"], weight = one(S), dest = 4),
#            (src = 4, isym = syms["d"], osym = syms["d"], weight = one(S), dest = 5)
#        ],
#        [1 => one(S)],
#        [5 => one(S)],
#        Σrange = 0:5,
#        Ωrange = 0:5
#    )
#
#    B = TensorFST(
#        [
#            (src = 1, isym = syms["a"], osym = syms["d"], weight = one(S), dest = 2),
#            (src = 2, isym = syms["ϵ"], osym = syms["e"], weight = one(S), dest = 3),
#            (src = 3, isym = syms["d"], osym = syms["a"], weight = one(S), dest = 4)
#        ],
#        [1 => one(S)],
#        [4 => one(S)],
#        Σrange = 0:5,
#        Ωrange = 0:5
#    )
#
#    C = TensorFST(
#        [
#            (src = 1, isym = syms["a"], osym = syms["d"], weight = one(S), dest = 2),
#            (src = 2, isym = syms["b"], osym = syms["e"], weight = one(S), dest = 3),
#            (src = 3, isym = syms["c"], osym = syms["ϵ"], weight = one(S), dest = 4),
#            (src = 4, isym = syms["d"], osym = syms["a"], weight = one(S), dest = 5)
#        ],
#        [1 => one(S)],
#        [5 => one(S)],
#        Σrange = 0:5,
#        Ωrange = 0:5
#    )
#    @test issame(C, compose(A, sort(B, inputlabel); connect = true))
#end
#
#@testset verbose=true "Weight-pushing" begin
#    S = TropicalSemiring{Float64}
#
#    A = TFST = TensorFST(
#        [
#            Arc(src=1, isym=1, osym=2, weight = one(S), dest=2),
#            Arc(src=1, isym=2, osym=1, weight = S(1),   dest=2),
#            Arc(src=1, isym=3, osym=2, weight = S(5),   dest=2),
#            Arc(src=1, isym=4, osym=1, weight = one(S), dest=3),
#            Arc(src=1, isym=5, osym=2, weight = S(1),   dest=3),
#            Arc(src=2, isym=5, osym=1, weight = one(S), dest=4),
#            Arc(src=2, isym=6, osym=2, weight = S(1),   dest=4),
#            Arc(src=3, isym=5, osym=1, weight = S(4),   dest=4),
#            Arc(src=3, isym=6 ,osym=2, weight = S(5),   dest=4),
#        ],
#        [(1,) => one(S)],
#        [(4,) => one(S)]
#    )
#
#    B = TFST = TensorFST(
#        [
#            Arc(src=1, isym=1, osym=2, weight = one(S), dest=2),
#            Arc(src=1, isym=2, osym=1, weight = S(1),   dest=2),
#            Arc(src=1, isym=3, osym=2, weight = S(5),   dest=2),
#            Arc(src=1, isym=4, osym=1, weight = S(4),   dest=3),
#            Arc(src=1, isym=5, osym=2, weight = S(5),   dest=3),
#            Arc(src=2, isym=5, osym=1, weight = one(S), dest=4),
#            Arc(src=2, isym=6, osym=2, weight = S(1),   dest=4),
#            Arc(src=3, isym=5, osym=1, weight = one(S), dest=4),
#            Arc(src=3, isym=6 ,osym=2, weight = S(1),   dest=4),
#        ],
#        [(1,) => one(S)],
#        [(4,) => one(S)]
#    )
#    @test issame(weightpush(A, toinitial), B)
#    @test issame(weightpush(A, tofinal), A)
#
#
#    PS = ProbSemiring{Float64}
#    C = TensorFST(
#        [
#            Arc(src=1, isym=1, osym=2, weight=zero(PS), dest=2),
#            Arc(src=1, isym=2, osym=1, weight=PS(1),    dest=2),
#            Arc(src=1, isym=3, osym=2, weight=PS(5),    dest=2),
#            Arc(src=1, isym=4, osym=1, weight=zero(PS), dest=3),
#            Arc(src=1, isym=5, osym=2, weight=PS(1),    dest=3),
#            Arc(src=2, isym=5, osym=1, weight=zero(PS), dest=4),
#            Arc(src=2, isym=6, osym=2, weight=one(PS),  dest=4),
#            Arc(src=3, isym=5, osym=1, weight=PS(4),    dest=4),
#            Arc(src=3, isym=6 ,osym=2, weight=PS(5),    dest=4),
#        ],
#        [(1,) => one(PS)],
#        [(4,) => one(PS)]
#    )
#
#    D = TensorFST(
#		[
#			Arc(src=1, isym=1, osym=2, weight=zero(PS), dest=2),
#			Arc(src=1, isym=2, osym=1, weight=PS(1/15), dest=2),
#			Arc(src=1, isym=3, osym=2, weight=PS(5/15), dest=2),
#			Arc(src=1, isym=4, osym=1, weight=zero(PS), dest=3),
#			Arc(src=1, isym=5, osym=2, weight=PS(9/15), dest=3),
#			Arc(src=2, isym=5, osym=1, weight=zero(PS), dest=4),
#			Arc(src=2, isym=6, osym=2, weight=one(PS),  dest=4),
#			Arc(src=3, isym=5, osym=1, weight=PS(4/9),  dest=4),
#			Arc(src=3, isym=6 ,osym=2, weight=PS(5/9),  dest=4),
#		],
#		[(1,) => PS(15)],
#		[(4,) => PS(1)]
#	)
#    E = TensorFST(
#        [
#            Arc(src=1, isym=1, osym=2, weight=zero(PS), dest=2),
#            Arc(src=1, isym=2, osym=1, weight=PS(1/6),  dest=2),
#            Arc(src=1, isym=3, osym=2, weight=PS(5/6),  dest=2),
#            Arc(src=1, isym=4, osym=1, weight=zero(PS), dest=3),
#            Arc(src=1, isym=5, osym=2, weight=one(PS),  dest=3),
#            Arc(src=2, isym=5, osym=1, weight=zero(PS), dest=4),
#            Arc(src=2, isym=6, osym=2, weight=PS(2/5),  dest=4),
#            Arc(src=3, isym=5, osym=1, weight=PS(4/15), dest=4),
#            Arc(src=3, isym=6 ,osym=2, weight=PS(5/15), dest=4),
#        ],
#        [(1,) => one(PS)],
#        [(4,) => PS(1/15)]
#    )
#    @test issame(weightpush(C, toinitial), D)
#    @test isapprox(weightpush(C, tofinal), E)
#end
#
@testset verbose=true "Libaries" begin
   include("linearfsts.jl")
#    include("ngramfsts.jl")
#    include("speechrecfsts.jl")
end

