
S = BoolSemiring

@testset failfast=true "LinearFSTs" begin
    X = ones(S, 2, 3)
    a = ones(S, size(X, 2) + 1)
    isymbolmap = [2, 3]
    osymbolmap = [3, 2]

    A = SparseTensorFSM{S,2}(
        [
            Arc(1, 2, one(S), 2, 3),
            Arc(1, 2, one(S), 3, 2),
            Arc(2, 3, one(S), 2, 3),
            Arc(2, 3, one(S), 3, 2),
            Arc(3, 4, one(S), 2, 3),
            Arc(3, 4, one(S), 3, 2),            
        ],
        [1 => one(S)],
        [4 => one(S)]
    )

    B = linearfst(X, isymbolmap, osymbolmap)

    @test issame(A, B)

    # A = TensorFST(
    #     [
    #         (src = (1,), isym = 2, osym = 2, weight = one(S), dest = (2,)),
    #         (src = (1,), isym = 3, osym = 3, weight = one(S), dest = (2,)),
    #         (src = (2,), isym = 2, osym = 2, weight = one(S), dest = (3,)),
    #         (src = (2,), isym = 3, osym = 3, weight = one(S), dest = (3,)),
    #         (src = (3,), isym = 2, osym = 2, weight = one(S), dest = (4,)),
    #         (src = (3,), isym = 3, osym = 3, weight = one(S), dest = (4,)),
    #     ],
    #     [(1,) => one(S)],
    #     [(4,) => one(S)],
    # )
    # @test issame(A, linearfst(X, isymbolmap))

    # A = TensorFST(
    #     [
    #         (src = (1,), isym = 2, osym = 3, weight = one(S), dest = (2,)),
    #         (src = (1,), isym = 3, osym = 2, weight = one(S), dest = (2,)),
    #         (src = (2,), isym = 2, osym = 3, weight = one(S), dest = (3,)),
    #         (src = (2,), isym = 3, osym = 2, weight = one(S), dest = (3,)),
    #         (src = (3,), isym = 2, osym = 3, weight = one(S), dest = (4,)),
    #         (src = (3,), isym = 3, osym = 2, weight = one(S), dest = (4,)),
    #     ],
    #     [(1,) => one(S), (2,) => one(S), (3,) => one(S), (4,) => one(S)],
    #     [(4,) => one(S)],
    # )
    # @test issame(A, linearfst(X, isymbolmap, osymbolmap; αval = a))

    # A = TensorFST(
    #     [
    #         (src = (1,), isym = 2, osym = 3, weight = one(S), dest = (2,)),
    #         (src = (1,), isym = 3, osym = 2, weight = one(S), dest = (2,)),
    #         (src = (2,), isym = 2, osym = 3, weight = one(S), dest = (3,)),
    #         (src = (2,), isym = 3, osym = 2, weight = one(S), dest = (3,)),
    #         (src = (3,), isym = 2, osym = 3, weight = one(S), dest = (4,)),
    #         (src = (3,), isym = 3, osym = 2, weight = one(S), dest = (4,)),
    #     ],
    #     [(1,) => one(S)],
    #     [(1,) => one(S), (2,) => one(S), (3,) => one(S), (4,) => one(S)],
    # )
    # @test issame(A, linearfst(X, isymbolmap, osymbolmap; ωval = a))
end
