
S = BoolSemiring

@testset failfast=true "SpeechRecognitionFSTs" begin
    lexicon = Dict(2 => [[2, 3, 5], [2, 3, 4]], 3 => [[2, 3, 4]])
    L = lexiconfst(S, lexicon)

    L2 = TensorFST(
        [
            (src = (1,), isym = 2, osym = 1, weight = one(S), dest = (2,)),
            (src = (2,), isym = 3, osym = 1, weight = one(S), dest = (3,)),
            (src = (3,), isym = 4, osym = 2, weight = one(S), dest = (4,)),
            (src = (3,), isym = 4, osym = 3, weight = one(S), dest = (5,)),
            (src = (3,), isym = 5, osym = 2, weight = one(S), dest = (6,)),
        ],
        [(1,) => one(S)],
        [(4,) => one(S), (5,) => one(S), (6,) => one(S)]
    )
    @test issame(L, L2)

    U = tokenfst(
        S,
        [(1, 2, 1), (2, 2, 1)],
        [1 => 1],
        [2 => 1],
        [2, 3, 4]
    )

    U2 = TensorFST(
        [
            # # Extra node
            # Source
            (src = (7,), isym = 0, osym = 4, weight = one(S), dest = (5,)),
            (src = (7,), isym = 0, osym = 3, weight = one(S), dest = (3,)),
            (src = (7,), isym = 0, osym = 2, weight = one(S), dest = (1,)),
            # Destination
            (src = (6,), isym = 0, osym = 0, weight = one(S), dest = (7,)),
            (src = (4,), isym = 0, osym = 0, weight = one(S), dest = (7,)),
            (src = (2,), isym = 0, osym = 0, weight = one(S), dest = (7,)),


            # Nodes tensorFST
            (src = (5,), isym = 6, osym = 0, weight = one(S), dest = (6,)),
            (src = (2,), isym = 2, osym = 0, weight = one(S), dest = (2,)),
            (src = (3,), isym = 4, osym = 0, weight = one(S), dest = (4,)),
            (src = (4,), isym = 4, osym = 0, weight = one(S), dest = (4,)),
            (src = (1,), isym = 2, osym = 0, weight = one(S), dest = (2,)),
            (src = (6,), isym = 6, osym = 0, weight = one(S), dest = (6,)),
			
        ],
        [(7,) => one(S)],
        [(7,) => one(S)]
    )
    @test issame(U, U2)
end


