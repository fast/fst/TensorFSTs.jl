# Script for converting between OpenFst and TensorFSTs

using OpenFst
using TensorFSTs
using Semirings

OF=OpenFst
TF=TensorFSTs
SR=Semirings

# Include a sign to applied to the OpenFst floating point weight
_SemiringToWeightType = Dict([
    (SR.LogSemiring{Float32,1}, (OF.LogWeight, -1)),
    (SR.LogSemiring{Float32,-1}, (OF.LogWeight, 1)),
    (SR.LogSemiring{Float64,1}, (OF.Log64Weight, -1)),
    (SR.LogSemiring{Float64,-1}, (OF.Log64Weight, 1)),
    (SR.LogSemiring{Float32,Inf}, (OF.TropicalWeight, -1)),
    (SR.LogSemiring{Float32,-Inf}, (OF.TropicalWeight, 1)),
    (SR.TropicalSemiring{Float32}, (OF.TropicalWeight, 1)),

])

# Converts from OpenFst weight to TensorFSTs semiring
_WeightToSemiringType = Dict([
    (OF.LogWeight, SR.LogSemiring{Float32,-1}),
    (OF.Log64Weight, SR.LogSemiring{Float64,-1}),
    (OF.TropicalWeight, SR.LogSemiring{Float32,-Inf})
])

# Extracts semiring floating point value with sgn correction
function _semiring_to_weight(s::S, sgn)::AbstractFloat where S <: SR.Semiring
    s.val * sgn
end

# If state s is final, return its final weight, else return zero
function final(A::TF.SparseTensorFSM{S,2}, s)  where S <: SR.Semiring
    fnd = findall(x->x==(s,),A.ω.coordinates)
    if isempty(fnd)
        return zero(S)
    else
        return A.ω.values[fnd][1]
    end
end

# Return the start state (the first if there are multiple)
function start(A::TF.SparseTensorFSM{S,2})  where S <: SR.Semiring
    A.α.coordinates[1][1]
end

# Return the states of the FST
function states(A::TF.SparseTensorFSM{S,2})  where S <: SR.Semiring
    states_list = []
    for arc in arcs(A)
        push!(states_list,arc.src)
        push!(states_list,arc.dest)
    end
    sort(unique(states_list))
end

# Return the arcs of the FST for state source q
function arcs_from_q(A::TF.SparseTensorFSM{S,2}, q::Integer)  where S <: SR.Semiring
    myarcs = []
    for arc in arcs(A)
        if arc.src[1]==q
            push!(myarcs,arc)
        end
    end
    return myarcs
end	

numstates(A::TF.SparseTensorFSM{S,2})  where S <: SR.Semiring = length(states(A))
numarcs(A::TF.SparseTensorFSM{S,2},q)  where S <: SR.Semiring = length(arcs_from_q(A,q))

function TF.SparseTensorFSM(ofst::OF.Fst{W}) where W <: OF.Weight
    S = _WeightToSemiringType[W] 
    myarcs = []
    for s in OF.states(ofst)
        for a in OF.arcs(ofst, s)
            push!(myarcs, 
                TF.Arc(convert(Int64, s), convert(Int64, a.nextstate), S(a.weight), convert(Int64, a.ilabel)-1, convert(Int64, a.olabel)-1),
            )
        end
    end

    tfst = TF.SparseTensorFSM{S,2}(myarcs, 
                        [convert(Int64, OF.start(ofst)) => S(1)], 
                        [convert(Int64,s) => S(OF.final(ofst, s)) for s in OF.states(ofst)])
	return tfst
end


function OF.VectorFst(tfst::TF.SparseTensorFSM{S,2}) where S <: SR.Semiring
    W, sgn = _SemiringToWeightType[S]
    ofst = OF.VectorFst{W}()
    # We need expanded for this line only
    OF.reservestates(ofst, numstates(tfst))
	inv_states_dict =  Dict(v[1]=>k for (k,v) in enumerate(states(tfst)))
    for (k, s1) in enumerate(states(tfst))
        s = s1[1]
        OF.addstate!(ofst)
        final_w = _semiring_to_weight(final(tfst, s), sgn)
        OF.setfinal!(ofst, k, final_w)
        OF.reservearcs(ofst, k, numarcs(tfst, s))
        for a in arcs_from_q(tfst, s)
            arc = OF.Arc(ilabel = a.sym[1]+1, 
                        olabel = a.sym[2]+1,
                        weight = _semiring_to_weight(a.weight, sgn), 
                        nextstate = inv_states_dict[a.dest[1]])
            OF.addarc!(ofst, k, arc)
        end
    end
    OF.setstart!(ofst, start(tfst))
    return ofst
end