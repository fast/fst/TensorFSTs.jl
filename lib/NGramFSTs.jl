# SPDX-License-Identifier: CECILL-2.1

module NGramFSTs

using ..TensorFSTs

export countngrams,
       ngramfst,
       read_arpafile,
       ngram_get_score,
       ngram_score_sentence

include("ngram_arpa.jl")


const BOS = 0
const EOS = -1

"""
    countngrams(seqs; N=3)

Return a the count of occurence for each ngram (encoded as `NTuple{N,Int}`).
`seqs` is a sequence of sequence of token ids. Special token ids `BOS = $BOS`
and `EOS = $EOS` are appended at the beginning and end of the sequence.
"""
countngrams

function countngrams(seqs; N = 3)
    counts = Dict()
    countngrams!.(Ref(counts), seqs; N = N)
    counts
end

function countngrams!(counts, seq; N=3)
    tokens = vcat(repeat([BOS], N-1), seq, [EOS])
    for i in 1:(length(tokens) - N + 1)
        ngram = tuple(tokens[i:i+N-1]...)
        counts[ngram] = get(counts, ngram, 0) + 1
    end
    counts
end


"""
    ngramfst(S::Semiring, counts)

Create N-gram FST from ngram counts.
"""
function ngramfst(S, ngrams::AbstractDict)
    states = Dict()
    for ngram in sort(collect(keys(ngrams)))
        prefix = ngram[1:end-1]
        suffix = ngram[2:end]

        all(id -> id == EOS, suffix) && continue

        #if (! all(id -> id == BOS, prefix)) && prefix ∉ keys(states)
        if prefix ∉ keys(states)
            states[prefix] = length(states) + 1
        end
        if last(suffix) != EOS && suffix ∉ keys(states)
            states[suffix] = length(states) + 1
        end
    end

    contextsize = length(first(keys(states)))
    init = ntuple(i -> BOS, contextsize)
    arcs, initweights, finalweights = [], [states[init] => S(1)], []
    for (ngram, w) in ngrams
        prefix = ngram[1:end-1]
        suffix = ngram[2:end]

        if last(suffix) == EOS
            push!(finalweights, states[prefix] => S(w))
        else
            arc = Arc(
                src = states[prefix],
                isym = suffix[end],
                osym = suffix[end],
                weight = S(w),
                dest = states[suffix]
            )
            push!(arcs, arc)
        end
    end

    TensorFST(arcs, initweights, finalweights)
end

end
