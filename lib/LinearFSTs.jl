# SPDX-License-Identifier: CECILL-2.1

module LinearFSTs

using ..TensorFSTs
# using SumSparseTensors

export linearfst


"""
    linearfst(X::AbstractArray{S}, isymbolmap, osymbolmap = isymbolmap; αval = one(S), ωval = one(S))

Construct a linear FST, i.e. a FST with transitions only between states `i` and
`i+1`.

If `X` is a ``p \\times q`` matrix, the function constructs a FST with ``q+1``
states with ``p`` transitions between adjacent states. The input symbol and
output symbol of the transition `X[i,j]` is given by `isymbolmap[i]` and
`osymbolmap[i]` respectively.

If `X` is a ``q``-dimensional vector, the function constructs a FST with
``q+1`` states with 1 transition between adjacent states. The input symbol and
the output symbol of the transition `X[i]` is given by `isymbolmap[i]` and
`osymbolmap[i]` respectively.

The keyword arguments `αval` and `ωval` specify the initial and final weight
for the initial and final states. If they are scalars, the initial and final
states are states ``1`` and ``q+1`` respectively. It is also possible to give a
``q+1`` array to specify per-state initial and final weight.
"""
function linearfst(X::AbstractArray{S}, isymbolmap, osymbolmap = isymbolmap; αval = one(S), ωval = one(S)) where S
    N = last(Base.size(X)) + 1
    nsym = Base.size(X)[1]

    arcs = Vector{Arc}(undef, length(X))
    #iterate over flatten X array
    for (i,x) in enumerate(vec(X))
        # row and column indexes (starts from 1)
        r = (i-1) % nsym +1
        c = (i-1) ÷ nsym + 1

        arcs[i] = Arc(c, c+1, x, isymbolmap[r], osymbolmap[r])
    end


    α = αval isa S ? [1 => αval] : [i => k for (i,k) in enumerate(αval) if k!=0]

    ω = ωval isa S ? [N => ωval] : [i => k for (i,k) in enumerate(ωval) if k!=0]

    SparseTensorFSM{S,2}(arcs, α, ω)
end

end
