# SPDX-License-Identifier: CECILL-2.1

module SpeechRecognitionFSTs

using ..TensorFSTs
using ..TensorFSTs.LinearFSTs

export EmissionMapping, lexiconfst, tokenfst

"""
    struct EmissionMapping <: AbstractMatrix{Int}
        numstates::Int
        numtokens::Int
    end

Default emission mapping which assumes the same number of emissions per token.
"""
struct EmissionMapping <: AbstractMatrix{Int}
    numpdfs::Int
    numtokens::Int
end

Base.size(x::EmissionMapping) = (x.numpdfs, x.numtokens)
Base.getindex(x::EmissionMapping, i, j) = (j - 1) * x.numpdfs + i
Base.minimum(x::EmissionMapping) = 1
Base.maximum(x::EmissionMapping) = (x.numpdfs * x.numtokens)



"""
    tokenfst(S, topo, initweights, finalweights, tokens, mapping)

Create a FST composed of a set of smaller FSTs. Each token share the same FST
topology specified with `topo = [(src, dest, weight), ...]`. `S` is a
`Semiring` of output FST, `tokens` is a list of token IDs, and
`mapping[tokens[i]]` is the output symbol .
"""
function tokenfst(
    S,
    topo,
    initweights,
    finalweights,
    tokens,
    mapping = nothing
)
    states = Set(Int[])
    arcs, init, final = [], [], []
    
    initdict = Dict(initweights...)
    finaldict = Dict(finalweights...)

    if isnothing(mapping)
        st = Set(Int[]) 
        for _ in tokens
            for top in topo
                push!(st, top[2])
                push!(st, top[1])
            end
        end
        mapping = EmissionMapping(length(st), length(tokens))   
    end

    # Extra state
    extra_state = length(mapping) + 1
    push!(states, extra_state)
    
    for (j, token) in enumerate(tokens)
        offset = length(states) - 1
        
        for topo_arc in topo
            
            src, dest, weight = offset + topo_arc[1], offset + topo_arc[2], topo_arc[3]

            arc = Arc(
                src = src,
                isym = mapping[topo_arc[2], j],
                osym = 0,
                dest = dest,
                weight = S(weight)
            )
            
            # Initialization arcs
            if topo_arc[1] in keys(initdict) 
                init_arc = Arc(
                    src = extra_state,
                    isym = 0,
                    osym = token,
                    dest = src,
                    weight = S(initdict[topo_arc[1]])
                )
                push!(arcs, init_arc)
            end
            if topo_arc[2] in keys(initdict)
                init_arc = Arc(
                    src = extra_state,
                    isym = 0,
                    osym = token,
                    dest = dest,
                    weight = S(initdict[topo_arc[2]])
                )
                push!(arcs, init_arc)
            end

            # Final arcs
            if topo_arc[1] in keys(finaldict)
                final_arc = Arc(
                    src = src,
                    isym = 0,
                    osym = 0,
                    dest = extra_state,
                    weight = S(finaldict[topo_arc[1]])
                )
                push!(arcs, final_arc)
            end
            if topo_arc[2] in keys(finaldict)
                final_arc = Arc(
                    src = dest,
                    isym = 0,
                    osym = 0,
                    dest = extra_state,
                    weight = S(finaldict[topo_arc[2]])
                )
                push!(arcs, final_arc)
            end

            push!(states, src)
            push!(states, dest)
            push!(arcs, arc)
        end    
    end
    
    push!(init, extra_state => one(S))
    push!(final, extra_state => one(S))

    TensorFST(collect(Set(arcs)), init, final)
end

include("trie.jl")
include("lexicon.jl")

end

