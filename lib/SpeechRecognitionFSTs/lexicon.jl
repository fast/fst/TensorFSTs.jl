# SPDX-License-Identifier: CECILL-2.1

"""
    lexiconfst(S, encoded_lexicon)

Create FST representation of lexicon.

All leafs of `encoded_lexicon` should be represented as `Integer`.

- `encoded_lexicon` - collection of `(token, [unit1, unit2, …])` pairs
- `S` - FST arc's weight type (e.g. Semiring)
# Example
```juliarepl
julia> lexicon = [1 => [1, 2, 3], 1 => [3,4,1], 2 => [1, 2]]
julia> lexiconfst(LogSemiring{Float32, 1.0}, lexicon)
TensorFST(
    …
)
```
"""
function lexiconfst(S, lexicon; ϵ_id=1)
    t = Trie{Int, Tuple}()
    prev_pron = []
    for (tid, pron) in sort(lexicon; by=p->p[end])
        if pron == prev_pron
            homonyms = (get(t[pron...])..., tid)
            t[pron...] = homonyms
        else
            t[pron...] = (tid,)
        end
        prev_pron = pron
    end
    _lexiconfst(S, t; ϵ_id=ϵ_id)
end

lexiconfst(S, lexicon::AbstractDict; ϵ_id=1) = lexiconfst(S, [wrd => pron for (wrd, prons) in lexicon for pron in prons])

_validpaths(t) = filter(p -> !ismissing(p[end]), collect(zip(keys(t), values(t))))
_emitosym(sub) = length(children(sub)) <= 1 && length(_validpaths(sub)) == 1
_isemitted(emitted, tokens, path) = haskey(emitted, tokens) && path == emitted[tokens]
function _lexiconfst(S, trie::AbstractTrie; ϵ_id=1)
    arcs = Arc{S,1}[]
    finals = Set{Int}()
    prefixs = Dict{Tuple, Int}(() => 1)
    nexts = 1

    root_path = ()

    if trie isa SubTrie
        root_path = trie.path
        path = Int[]
        for p in root_path
            prevs = prefixs[tuple(path...)]
            nexts += 1
            arc = Arc(src=prevs, dest=nexts, isym=p, osym=ϵ_id, weight=one(S))
            push!(arcs, arc)

            push!(path, p)
            prefixs[tuple(path...)] = nexts
        end
    end

    # Store emitted symbols and corresponding paths
    emitted = Dict{Tuple, Tuple}()
    root_l = length(root_path)

    for (path, tokens) in Iterators.drop(trie, 1)
        ppath = path[1:end-1] # prefix path

        isym = path[end]
        osyms = (ϵ_id,)
        sub = trie[path[root_l+1:end]...] # subtrie
        prev_sub = trie[ppath[root_l+1:end]...] # previous sub

        if _emitosym(sub) && !_emitosym(prev_sub)
            # Emitting output symbol as soon as possible
            vp = _validpaths(sub)[1]
            osyms = vp[end]
            emitted[osyms] = vp[1]
        elseif !ismissing(tokens) && !_isemitted(emitted, tokens, path)
            # Emitting at the end
            osyms = tokens
            emitted[osyms] = path
        end

        prevs = prefixs[ppath]
        if prevs in finals
            nexts += 1
            prevs = prefixs[ppath[1:end-1]]
            arc = Arc(src=prevs, dest=nexts, isym=ppath[end], osym=ϵ_id, weight=one(S))
            push!(arcs, arc)

            prevs = nexts
            prefixs[ppath] = nexts # update
        end

        for osym in osyms
            nexts += 1
            arc = Arc(src=prevs, dest=nexts, isym=isym, osym=osym, weight=one(S))
            push!(arcs, arc)

            if !ismissing(tokens)
                push!(finals, nexts)
            end
        end

        prefixs[path] = nexts
    end
    TensorFST(
        arcs,
        [1 => one(S)],
        [fs => one(S) for fs in finals]
    )
end
