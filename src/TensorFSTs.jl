# SPDX-License-Identifier: CECILL-2.1

module TensorFSTs

using Semirings
using LinearAlgebra
using SparseSemiringAlgebra

# Generic FSM API
export AbstractFSMArc,
       Arc,
       AbstractFSM,
       arcs,
       states,
       initweight,
       finalweight,
       draw,
       resize,
       ϵ

include("abstractfsm.jl")


export SparseTensorFSM

include("sparsetensorfsm.jl")


export fsmcat,
       fsmclosure,
       fsmcompose,
       fsmpermute,
       fsmproject,
       fsmreverse,
       fsmunion,
       fsmshortestdistance

include("ops.jl")

## standalone libraries
include("../lib/LinearFSTs.jl")
#include("../lib/NGramFSTs.jl")
#include("../lib/SpeechRecognitionFSTs/SpeechRecognitionFSTs.jl")

end

