# SPDX-License-Identifier: CECILL-2.1

"""
    SparseTensorFSM{T,N} <: AbstractFSM{T,N}

`N`-order Finite-State machine over the weight type `T` encoded as a sparse
tensor.
"""
struct SparseTensorFSM{T,N} <: AbstractFSM{T,N}
    M::AbstractSparseArray{<:MatrixSemiring,N}
    α::AbstractSparseVector{T}
    ω::AbstractSparseVector{T}

    function SparseTensorFSM{T,N}(M::AbstractSparseArray{<:MatrixSemiring,N},
                                  α::AbstractSparseVector{T},
                                  ω::AbstractSparseVector{T}) where {T,N}
        size(α) ≠ size(ω) && throw(ArgumentError("`α` and `ω` should have the same size"))
        new{T,N}(M, α, ω)
    end
end


function SparseTensorFSM{T,N}(arcs, initweights, finalweights) where {T,N}
    numstate = 0
    for arc in arcs numstate = max(numstate, max(arc.src, arc.dest)) end
    for (state, weight) in initweights numstate = max(numstate, state) end
    for (state, weight) in finalweights numstate = max(numstate, state) end

    K = Dims{N}
    V = Tuple{Vector{Dims{2}},Vector{T}}
    sym_matrices = Dict{K,V}()
    for arc in arcs
        coordinates, values = get(sym_matrices, arc.sym, (Dims{2}[], T[]))
        push!(coordinates, (arc.src, arc.dest))
        push!(values, arc.weight)
        sym_matrices[arc.sym] = (coordinates, values)
    end

    Mcoordinates = collect(keys(sym_matrices))
    Mvalues = SparseArray{T,2}[]
    for (coordinates, values) in Base.values(sym_matrices)
        Mi = sparse(coordinates, values, numstate, numstate)
        push!(Mvalues, Mi)
    end
    Mvalues = MatrixSemiring{numstate,eltype(Mvalues)}[MatrixSemiring(v) for v in Mvalues]

    M = sparse(Mcoordinates, Mvalues)
    α = sparsevec(map(first, initweights), map(last, initweights), numstate)
    ω = sparsevec(map(first, finalweights), map(last, finalweights), numstate)

    SparseTensorFSM{T,N}(M, α, ω)
end


function arcs(fsa::SparseTensorFSM{T,N}) where {T,N}
    arcs = Arc{T,N}[]
    for nzi in 1:nnz(fsa.M)
        sym = fsa.M.coordinates[nzi]
        Mi = val(fsa.M.values[nzi])
        for nzi in 1:nnz(Mi)
            (src, dest) = Mi.coordinates[nzi]
            weight = Mi.values[nzi]
            push!(arcs, Arc(src, dest, weight, sym...))
        end
    end
    arcs
end

states(fsa::SparseTensorFSM) = 1:size(fsa.α, 1)
initweight(fsa::SparseTensorFSM, state) = fsa.α[state]
finalweight(fsa::SparseTensorFSM, state) = fsa.ω[state]


function resize(fsm::SparseTensorFSM{T,N}, size::Dims{N}) where {T,N}
    M = sparse(fsm.M.coordinates, fsm.M.values, size...)
    SparseTensorFSM{T,N}(M, fsm.α, fsm.ω)
end

Base.zero(::Type{SparseTensorFSM{T,N}}) where {T,N} = SparseTensorFSM{T,N}(Arc{T,N}[], Pair{Int,T}[], Pair{Int,T}[])
Base.zero(fsm::SparseTensorFSM) = zero(typeof(fsm))
Base.one(::Type{SparseTensorFSM{T,N}}) where {T,N} = SparseTensorFSM{T,N}(Arc{T,N}[], [1 => one(T)], [1 => one(T)])
Base.one(fsm::SparseTensorFSM) = one(typeof(fsm))


