# SPDX-License-Identifier: CECILL-2.1


"""
    const ϵ = 1

Index of the epsilon label (i.e. empty sequence). Every other label will in a
state-machine will have an index greated than `ϵ`.
"""
const ϵ = 1


"""
    Arc{T,N}

Arc of a `N`-order relation finite-state machine over the semiring `T`.

# Constructor
    Arc(src, dest, weight, sym...)

# Examples
```jldoctest
julia> Arc(1, 1, one(BoolSemiring), ϵ)
Arc{BoolSemiring, 1}(1, 1, true, (1,))

julia> Arc(2, 3, ProbSemiring{Float32}(1/2), 2, 3)
Arc{ProbSemiring{Float32}, 2}(2, 3, 0.5, (2, 3))
```
"""
struct Arc{T,N}
    src::Int
    dest::Int
    weight::T
    sym::Dims{N}
end


Arc(src, dest, weight, sym::Vararg{Int,N}) where N = Arc{typeof(weight),N}(src, dest, weight, sym)


"""
    abstract type AbstractFSM{T} end

Supertype for finite-state machines. `T` is the semiring over which the machine
is defined and `N` is the order of the relation represented by the machine.
"""
abstract type AbstractFSM{T,N} end


"""
    arcs(fsm::AbstractFSM)

Return an iterable over the arcs of `fsm`.
"""
arcs


"""
    states(fsm::AbstractFSM)

Return an iterable over the states of `fsm`.
"""
states


"""
    initweight(fsm::AbstractFSM, state)

Return the initial weight associated to `state` in `fsm`.
"""
initweight


"""
    finalweight(fsm::AbstractFSM, state)

Return the final weight associated to `state` in `fsm`.
"""
finalweight


#==============================================================================
Drawing FST with graphviz.
==============================================================================#

struct DrawFSM
    fsm
    symbols
    show_zero_arcs
end


"""
    draw(fsm[, symbols...]; show_zero_arcs)

Return a drawable representation of the finite-state machine `fsm` using
graphviz. `symbols` is a mapping from integer to string representation. If only
one symbol mapping is provided it is used for every label dimension.
"""
draw

draw(fsm::AbstractFSM{T,N}; show_zero_arcs = false) where {T,N} =
    DrawFSM(fsm, ntuple(n -> Dict(), N), show_zero_arcs)

draw(fsm::AbstractFSM{T,N}, symbols; show_zero_arcs = false) where {T,N} =
    DrawFSM(fsm, ntuple(n -> symbols, N), show_zero_arcs)

draw(fsm::AbstractFSM{T,N}, symbols::Vararg{Any,N}; show_zero_arcs = false) where {T,N} =
    DrawFSM(fsm, symbols, show_zero_arcs)


function Base.show(io::IO, ::MIME"image/svg+xml", dfsm::DrawFSM)
    # Generate the graph description in the DOT language.
    buffer = IOBuffer()
    ioctx = IOContext(buffer, :compact => true)
    _write_dot(ioctx, dfsm.fsm, dfsm.symbols, dfsm.show_zero_arcs)
    fst_dot = String(take!(buffer))

    # Generate the SVG file from the dot script.
    dir = mktempdir()
    dotpath = joinpath(dir, "fst.dot")
    svgpath = joinpath(dir, "fst.svg")
    open(f -> write(f, fst_dot), dotpath, "w")
    run(`dot -Tsvg $(dotpath) -o $(svgpath)`)
    write(io, open(f -> read(f, String), svgpath))
end


function _write_dot(io::IO, fsm::AbstractFSM{T,N}, symbols, show_zero_arcs) where {T,N}
    println(io, "digraph {")
    println(io, "  rankdir=LR;")

    for q in states(fsm)
        iw = initweight(fsm, q)
        fw = finalweight(fsm, q)

        if ! iszero(iw)
            style = "bold"
            buffer = IOBuffer()
            printstyled(IOContext(buffer, :compact => true), iw)
            label = isone(iw) ? "$q" : "$(String(take!(buffer)))/$(q)"
        else
            style = "solid"
            label = "$q"
        end

        if ! iszero(fw)
            shape = "doublecircle"
            buffer = IOBuffer()
            printstyled(IOContext(buffer, :compact => true), fw)
            label *= isone(fw) ? "" : "/$(String(take!(buffer)))"
        else
            shape = "circle"
        end

        println(io, "  $q [label=\"$(label)\" shape=\"$(shape)\" style=\"$style\"];")
    end

    for arc in arcs(fsm)
        if ! iszero(arc.weight) || show_zero_arcs
            label = join([get(symbols[n], arc.sym[n], arc.sym[n]) for n in 1:N], ":")
            if ! iszero(arc.weight) && ! isone(arc.weight)
                buffer = IOBuffer()
                printstyled(IOContext(buffer, :compact => true), arc.weight)
                label *= "/$(String(take!(buffer)))"
            end
            color = iszero(arc.weight) ? "#8080804D" : "black"
            style = iszero(arc.weight) ? "dashed" : "solid"
            println(io, "  $(arc.src) -> $(arc.dest) [label=\"$(label)\" color=\"$(color)\" style=\"$(style)\"];")
        end
    end

    println(io, "}")
end

