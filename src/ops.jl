# SPDX-License-Identifier: CECILL-2.1


"""
    fsmunion(A::AbstractFSM{T,N}, B::AbstractFSM{T,N}) where {T,N}
    A + B

Compute the union (also denoted sum) of two weighted automata `A` and `B`.

The union of two ``N``-order weighted automata ``A`` and ``B`` over the
semiring ``(S, \\oplus, \\otimes, \\bar{0}, \\bar{1})``, denoted ``A+B``, is
defined such that for all ``x \\in (\\Sigma^*_1 \\times \\dots \\times \\Sigma^*_N)``
```math
    (A + B)(x) = A(x) \\oplus B(x)
```

## Complexity
* Time: ``O(V_1 + E_1 + V_2 + E_2)``
* Depth: ``O(1)``
where ``V_i`` is the number of states and ``E_i`` is the number of arcs of the
``i``-th automaton.

See also [`fsmcat`](@ref) and [`fsmclosure`](@ref).
"""
function fsmunion(A::AbstractFSM{T,N}, B::AbstractFSM{T,N}) where {T,N}
    newsize = ntuple(n -> max(size(A.M, n), size(B.M, n)), N)
    A, B = resize(A, newsize), resize(B, newsize)

    M = A.M .⊕ B.M
    α = vcat(A.α, B.α)
    ω = vcat(A.ω, B.ω)
    typeof(A)(M, α, ω)
end

Base.:+(A::AbstractFSM, B::AbstractFSM) = fsmunion(A, B)


"""
    fsmcat(A::AbstractFSM{T,N} B::AbstractFSM{T,N}) where {T,N}
    A * B

Compute the concatenation (also denoted product) of two weighted automata `A`
and `B`.

The concatenation of two ``N``-order weighted automata ``A`` and ``B`` over the
semiring ``(S, \\oplus, \\otimes, \\bar{0}, \\bar{1})``, denoted ``A \\cdot B``
or ``AB``, is defined such that for all ``x, y \\in (\\Sigma^*_1 \\times \\dots \\times \\Sigma^*_N)``
```math
    (A \\cdot B)(z) = \\bigoplus_{z = xy} A(z) \\otimes B(z)
```
where ``xy = (x_1y_1, \\dots, x_Ny_N)``.

## Complexity
* Time: ``O(V_1 + E_1 + V_2 + E_2 + F_1 I_2)``
* Depth: ``O(1)``
where ``V_i`` is the number of states, ``E_i`` is the number of arcs, ``I_i``
is the number of initial states and ``F_i`` is the number of final states of
the ``i``-th automaton.

See also [`fsmunion`](@ref) and [`fsmclosure`](@ref).
"""
fsmcat

function fsmcat(A::AbstractFSM{T,N}, B::AbstractFSM{T,N}) where {T,N}
    newsize = ntuple(n -> max(size(A.M, n), size(B.M, n)), N)
    A, B = resize(A, newsize), resize(B, newsize)

    M = A.M .⊕ B.M
    ω̄ = vcat(A.ω, zero(T) * B.ω)
    ᾱ = vcat(zero(T) * A.α, B.α)
    M[1] += MatrixSemiring(ω̄ * transpose(ᾱ))

    α = vcat(A.α, zero(T) * B.α)
    ω = vcat(A.ω * zero(T), B.ω)
    typeof(A)(M, α, ω)
end

Base.:*(A::AbstractFSM, B::AbstractFSM) = fsmcat(A, B)


"""
    fsmclosure(A::AbstractFSM; closure_plus = false)
    A^* # equivalent to fsmclosure(A; closure_plus = false)
    A^+ # equivalent to fsmclosure(A; closure_plus = true)

Compute the Kleene closure (also denoted *star*) of a weighted automaton `A`
or the Kleene plus operation if `closure_plus` is `true`.

The Kleene closure of a ``N``-order weighted automaton ``A`` over the
semiring ``(S, \\oplus, \\otimes, \\bar{0}, \\bar{1})``, denoted ``A^*``, is defined
such that for all ``x \\in (\\Sigma^*_1 \\times \\dots \\times \\Sigma^*_N)``
```math
    A^*(x) = \\bigoplus^{+\\infty}_{n = 0} A^n(x).
```
where ``A^n(x) = \\underbrace{A(x) A(x) \\dots A(x)}_{n \\text{ times}}`` is
the concatenation (see [`fsmcat`](@ref)) of ``A`` with itself ``n`` times and
``A^0(x)`` is defined such that
```math
A^0(x) = \\begin{cases}
\\bar{1} & x = (\\epsilon, \\dots, \\epsilon) \\\\
\\bar{0} & \\text{otherwise}.
\\end{cases}
```

The Kleene plus operation, denoted ``A^+``, is defined similarly to the Kleene
closure except that the ``\\oplus`` summation starts from ``1``:
```math
    A^+(x) = \\bigoplus^{+\\infty}_{n = 1} A^n(x).
```

## Complexity
* Time: ``O(V + E + F I)``
* Depth: ``O(1)``
where ``V`` is the number of states, ``E`` is the number of arcs, ``I`` is the
number of initial states and ``F`` is the number of final states of the
automaton ``A``.

See also [`fsmunion`](@ref) and [`fsmcat`](@ref).
"""
fsmclosure

function _fsmclosure_plus(A::AbstractFSM)
    M = copy(A.M)
    M[1] = MatrixSemiring(A.ω * transpose(A.α))
    typeof(A)(M, A.α, A.ω)
end

_fsmclosure(A::AbstractFSM) = one(A) * _fsmclosure_plus(A)

fsmclosure(A::AbstractFSM; closure_plus = false) = closure_plus ? _fsmclosure_plus(A) : _fsmclosure(A)

Base.:^(A::AbstractFSM, ::typeof(+)) = fsmclosure(A; closure_plus = true)
Base.:^(A::AbstractFSM, ::typeof(*)) = fsmclosure(A; closure_plus = false)


"""
    fsmpermute(A::AbstractFSM, p)

Permute the label dimensions of the automaton `A` according to the permutation
`p`.

The permutation of the arcs' label of a ``N``-order weighted automaton ``A``
over the semiring ``(S, \\oplus, \\otimes, \\bar{0}, \\bar{1})`` given a
permutation ``p = (p_1, \\dots, p_N)`` is defined such that for all
``x \\in (\\Sigma^*_1 \\times \\dots \\times \\Sigma^*_N)``:
```math
    \\text{permute}_p(A)(x) = A(x')
```
where ``x' = (x_{p_1}, \\dots, x_{p_N})`` is the permutation of ``x`` given ``p``.

## Complexity
* Time: ``O(E)``
* Depth: ``O(1)``
where ``E`` is the number of arcs of the automaton ``A``.

See also [`fsmproject`](@ref) and [`fsmreverse`](@ref).
"""
fsmpermute(A::AbstractFSM, p) = typeof(A)(permutedims(A.M, p), A.α, A.ω)


"""
    fsmproject(A::AbstractFSM; dims)

Project the label dimensions `dims` of the automaton `A` onto the other label
dimensions.

The projection of a ``N``-order weighted automaton ``A`` over the
semiring ``(S, \\oplus, \\otimes, \\bar{0}, \\bar{1})`` of the label dimensions
``I \\subset \\{1, \\dots, N\\}`` onto ``I^{\\complement}``  is defined such that for all
``x \\in (\\Sigma^*_1 \\times \\dots \\times \\Sigma^*_N)``:
```math
    \\text{project}_I(A)(x_{\\setminus I}) = \\bigoplus_{\\{ z \\: : \\: z_{\\setminus I} = x_{\\setminus I} \\}} A(z)
```
where ``x_{\\setminus I} = (x_i \\: : \\: i \\notin I)`` is the tuple of
elements in ``x`` with index not in ``I``.

## Complexity
* Time: ``O(E)``
* Depth: ``O(1)``
where ``E`` is the number of arcs of the automaton ``A``.

See also [`fsmpermute`](@ref) and [`fsmreverse`](@ref).
"""
fsmproject

_project(fsm::AbstractFSM{T,N}, dims::Dims{M}) where {T,N,M} =
    SparseTensorFSM{T,N-M}(dropdims(sum(fsm.M; dims); dims), fsm.α, fsm.ω)

_project(fsm::AbstractFSM, dims::Integer) = _project(fsm, tuple(dims))

fsmproject(fsm; dims) = _project(fsm, dims)


"""
    fsmreverse(A::AbstractFSM)

Reverse the direction of the arcs in the automaton `A`.

The reversal of a ``N``-order weighted automaton ``A`` over the semiring
``(S, \\oplus, \\otimes, \\bar{0}, \\bar{1})``, denoted ``A^R``, is defined
such that for all ``x \\in (\\Sigma^*_1 \\times \\dots \\times \\Sigma^*_N)``:
```math
    A^R(x) = A(x^R)
```
where ``x' = (x_1^R, \\dots, x_{N}^R)`` is the tuple of reversed sequences
``x_i^R``.

## Complexity
* Time: ``O(E)``
* Depth: ``O(1)``
where ``E`` is the number of arcs of the automaton ``A``.
See also [`fsmpermute`](@ref) and [`fsmproject`](@ref).
"""
fsmreverse(A::AbstractFSM) = typeof(A)(transpose.(A.M), A.ω, A.α)


#@enum FSTPush toinitial tofinal
#
#"""
#weightpush(A, toinitial | tofinal)
#
#Returns a FST ``B`` with the weights of each path pushed as much as possible towards the initial/final states.
#"""
#function weightpush(A::TensorFST{S}, p::FSTPush) where S
#
#	dims = dimensions(A)
#	N = length(dims.src)
#
#    nt = ntuple(identity, N)
#
#	# Shortest distance from q ∈ Q to F
#	if p == toinitial
#		d = mulsumstar(A.M, A.ω, dims.dest, dims.src)
#        oneD = ones(S, size(d)...)
#
#        M = mul(map(inv, d), A.M, d, A.dims.src, A.dims.dest)
#		α = mul(oneD, A.α , d, nt, nt)
#		ω = mul(map(inv, d), A.ω, oneD, nt, nt)
#	# Shortest distance from I to q ∈ Q
#	else
#		d = mulsumstar(A.α, A.M, dims.src, dims.dest)
#        oneD = ones(S, size(d)...)
#
#        M = mul(d, A.M, map(inv, d), A.dims.src, A.dims.dest)
#		α = mul(d, A.α , oneD, nt, nt)
#		ω = mul(oneD, A.ω, map(inv, d), nt, nt)
#	end
#	TensorFST{S}(dims, M, α, ω)
#end
#
#
#@enum FSTSort state inputlabel outputlabel
#
#"""
#    sort(A, state | inputlabel | outputlabel)
#
#Return a FST ``B`` with the arcs sorted according to the state, the input label,
#or the output label.
#"""
#function Base.sort(A::TensorFST{S}, s::FSTSort) where S
#    dims = dimensions(A)
#
#    sdim = if s == state
#        first(dims.src)
#    elseif s == inputlabel
#        dims.isym
#    else
#        dims.osym
#    end
#
#    TensorFST{S}(dims, sort(A.M, sdim), A.α, A.ω)
#
#end
#
#"""
#    weight(A)
#
#Compute the weight ``W(A)`` defined as
#```math
#\\forall (x, y) \\in \\Sigma^* \\times \\Delta^*, \\; A^* (x, y) = \\bigoplus_{x,y} A(x,y)
#```
#"""
#weight(A::TensorFST) = dotstar(A.α, A.M, A.ω, dimensions(A).src, dimensions(A).dest)


"""
    fsmcompose(A::AbstractFSM{T}, B::AbstractFSM{T}) where T
    A ∘ B

Compute the composition of the automata ``A`` and ``B``.

The composition of a ``M``-order weighted automaton ``A`` and a ``N``-order
weighted automaton ``B`` over the semiring ``(S, \\oplus, \\otimes, \\bar{0}, \\bar{1})``,
denoted ``A \\circ B``, is defined such that for all
``x \\in (\\Sigma^*_1 \\times \\dots \\times \\Sigma^*_{N-1})`` and
``y \\in (\\Omega^*_2, \\dots, \\Omega^*_N)``:
```math
    (A \\circ B)(x, y) = \\bigoplus_{\\delta \\in \\Delta^*} A(x, \\delta) \\otimes B(\\delta, y)
```
where ``\\Delta`` is the set of symbols for the last label dimension of ``A``
and the first label dimension of ``B``.
"""
fsmcompose

function _compose_M_leftϵ_first(
		x::AbstractSparseArray{MatrixSemiring{P,Tm},Nx},
		y::AbstractSparseArray{MatrixSemiring{Q,Tm},Ny}
	) where {P,Q,Tm,Nx,Ny}

	x = sort(x, Nx) #M^A
	y = sort(y, 1)  # M^B

	N = Nx + Ny - 2
	coordinates = Dims{N}[]
	values = MatrixSemiring{P*Q,Tm}[]

    # sum(X[:,ϵ])
	yϵ = zero(eltype(y))
	for ynzi in y.ptr[1]:y.ptr[2]-1
		yval = y.values[ynzi]
		yϵ += yval
	end

	# 1. Composition of the ϵ label of the left fsm
	𝒩y = MatrixSemiring(nullspace(val(yϵ); keepdims = true))
	for xnzi in x.ptr[ϵ]:x.ptr[ϵ+1]-1
		xcoo, xval = x.coordinates[xnzi], x.values[xnzi]
		push!(coordinates, (xcoo[1:end-1]..., 1))
		push!(values, xval ⊗ 𝒩y)
	end

	# 2. Composition of the ϵ label (right side).
	Ix = one(eltype(x))
	for ynzi in y.ptr[1]:y.ptr[2]-1
		ycoo, yval = y.coordinates[ynzi], y.values[ynzi]

		push!(coordinates, (1, ycoo[2:end]...))
		push!(values, Ix ⊗ yval)
	end

	# Composition of the other labels
	for s in 2:min(size(x, Nx), size(y, Ny))
		for xnzi in x.ptr[s]:x.ptr[s+1]-1, ynzi in y.ptr[s]:y.ptr[s+1]-1
			xcoo, xval = x.coordinates[xnzi], x.values[xnzi]
			ycoo, yval = y.coordinates[ynzi], y.values[ynzi]

			push!(coordinates, (xcoo[1:end-1]..., ycoo[2:end]...))
			push!(values, xval ⊗ yval)
		end
	end

	sparse(coordinates, values, size(x)[1:end-1]..., size(y)[2:end]...)
end

function fsmcompose(A::AbstractFSM{T,Na}, B::AbstractFSM{T,Nb}) where {T,Na,Nb}
    M = _compose_M_leftϵ_first(A.M, B.M)
	SparseTensorFSM{T,Na+Nb - 2}(M, kron(A.α, B.α), kron(A.ω, B.ω))
end

function _has_changed(x, y)
    changed = false
    for i in eachindex(x)
        if ! (val(x[i]) ≈ val(y[i]))
            changed = true
            break
        end
    end
    changed
end

#"""
#    shortestdistance(A)
#
#Compute the single-source shortest distance from the "meta" initial set  to all
#states of ``A`` such as
#```math
#d[q] = \\bigoplus_{\\pi \\in A} \\alpha(p[\\pi]) w[\\pi]
#```
#"""
#shortestdistance(A::TensorFST) = mulsumstar(A.α, A.M, dimensions(A).src, dimensions(A).dest)

function fsmshortestdistance(A::AbstractFSM{T,N}) where {T,N}
	N_s = size(val(A.M[1,1]))[1] # I AM SO UGLY

    # 1, Sum over arcs with the same source and destination states
	M = zeros(T,N_s,N_s)
	for m in A.M
		M+=val(m)
	end

    # 2. Sum over all paths
    u_n = transpose(A.α)
    res = similar(u_n)
    prevres = similar(u_n)
    copyto!(res, u_n)
    copyto!(prevres, u_n)
    stop = false
    while ! stop
        u_n = u_n*M
        res += u_n
        stop = !_has_changed(res, prevres)
        copyto!(prevres, res)
    end
    res	
end
