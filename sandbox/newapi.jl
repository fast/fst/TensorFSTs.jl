### A Pluto.jl notebook ###
# v0.19.36

using Markdown
using InteractiveUtils

# ╔═╡ c893a80a-83cd-11ee-1a82-d7c85de7ab7a
begin
	using Pkg
	Pkg.activate("..")

	using Revise
	# Once the FAST registry has been added, just run this cell and these
	# packages will be install for this notebook automatically.
	using Semirings
	using TensorFSTs
end

# ╔═╡ a4127267-40fd-4550-9446-6d82b0ae8373
md"""# TensorFST
*Lucas ONDEL YANG, LISN CNRS, 24/11/2023*

This notebook is a *very brief* introduction of [TensorFSTs.jl](https://gitlab.lisn.upsaclay.fr/fast/fst/TensorFSTs.jl). 

TensorFSTs.jl is a [Julia](https://julialang.org/) package, it is open-source and provided under the [CeCILL 2.1 license](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html).
"""

# ╔═╡ 701e9298-89fb-4082-8268-a57bd4cda574
md"""
## Installation

TensorFSTs.jl is accessible through the [FAST registry](https://gitlab.lisn.upsaclay.fr/fast/registry). To add this registry, enter the package mode of the Julia REPL and type:

```julia
pkg> registry add "https://gitlab.lisn.upsaclay.fr/fast/registry"
```

"""

# ╔═╡ f781cf77-3332-48f8-b2dc-4d4880c81a08
md"""
## Creating an FST
"""

# ╔═╡ 4684d1ff-7a80-4e8e-9f48-f3ef5b11cb5a
S = LogSemiring{Float64,1}

# ╔═╡ b4279331-1f8c-49b7-9bc4-b46f912ace46
md"""
## Visualizing an FST
"""

# ╔═╡ d62946ec-112e-40c4-8b8c-24dd6f1c5103
md"""
The function `summary(fst)` return some basic information about your fst. 
"""

# ╔═╡ f779c388-223c-4299-9523-3f3b3ba21769
md"""
To represent the FST as an directed acyclic graph first you can use the `draw(fst)` function.
"""

# ╔═╡ 6f79ca2f-9c47-49a6-823f-426f4abb6999
md"""
As you can see, the input and output symbols are represented by integer internally. Usually you will want to display specific symbols rather than plain digits. To do so you need to create a symbol mapping, i.e. a dictionary with key integer and the associated textual representation.
"""

# ╔═╡ 72d16117-5949-4260-9549-8e5773c37148
isymbols = Dict(
	1 => "ϵ",
	2 => "a",
	3 => "b",
	4 => "c"
)

# ╔═╡ 240ee7f9-c1aa-4d46-804a-55b30fcd2249
osymbols = Dict(
	1 => "ϵ",
	2 => "u",
	3 => "v",
	4 => "w"
)

# ╔═╡ d048d3ee-98e0-4fbc-a6cb-c34adc8e9025
md"""
!!! info
	The symbol id `1` is reserved for the empty sequence usually represented as `ϵ`. When you define a symbol mapping, make sure not to assign a meaningful symbol to `1`, e.g. `ϵ`, or `<eps>`, ... 
"""

# ╔═╡ 4224ebb5-9a46-415e-8a3f-0231ff2b1704
md"""
## Rational operation

To build complex FST, we need only 3 operations called "rational operations":
* union
* concatenation
* (Kleene) closure, also called *star* operation.
"""

# ╔═╡ 9d584247-568b-4378-8d1e-8a8ca8e113ed
md"""
## Creating a new semiring

Creating a new semiring is important as it allows to defines machine tailored to your tasks. Here we'll create a semiring over FST.
"""

# ╔═╡ 17483a0e-9633-46b3-aed0-8ba955eeb515
# ╠═╡ disabled = true
#=╠═╡
begin
	struct FSTSemiring{S} <: Semiring
		val::TensorFST{S}
	end
	
	Base.zero(::Type{FSTSemiring{S}}) where S = 
		FSTSemiring(TensorFST(Arc{S}[], Pair{Int,S}[], Pair{Int,S}[]))

	function Base.one(::Type{<:FSTSemiring{S}}) where S
		FSTSemiring(TensorFST(
			[Arc(src=1, dest=2, isym=1, osym=1, weight=one(S))],
			[1 => one(S)],
			[2 => one(S)]
		))
	end

	Semirings.:⊕(x::FSTSemiring, y::FSTSemiring) = 
		FSTSemiring(union(val(x), val(y)))
	Semirings.:⊗(x::FSTSemiring, y::FSTSemiring) = 
		FSTSemiring(cat(val(x), val(y)))
	Base.:^(x::FSTSemiring, ::typeof(*)) = FSTSemiring(closure(val(x)))
end
  ╠═╡ =#

# ╔═╡ ebe54b31-daa5-4970-9a25-4146d9f101d8
#=╠═╡
A = TensorFST(
	[
		(src = 1, isym = 2, osym = 2, weight = one(S), dest = 1),
		(src = 1, isym = 2, osym = 2, weight = one(S), dest = 2),
		(src = 2, isym = 2, osym = 2, weight = one(S), dest = 2),
		(src = 2, isym = 2, osym = 2, weight = one(S), dest = 3),
		(src = 3, isym = 2, osym = 2, weight = one(S), dest = 3)
	],
	[1 => one(S)],
	[3 => one(S)]
)
  ╠═╡ =#

# ╔═╡ 27d2c155-518c-49fc-b273-0853f7a04c75
#=╠═╡
summary(A)
  ╠═╡ =#

# ╔═╡ 41df922b-543f-433e-9e30-b6863ca00366
#=╠═╡
draw(A; isymbols, osymbols)
  ╠═╡ =#

# ╔═╡ 2a48bcea-91cf-4c85-8362-c25eaefa965f
#=╠═╡
draw(closure(A); isymbols, osymbols)
  ╠═╡ =#

# ╔═╡ 7e2dec0f-a92a-4b39-96dd-94d6f61519d8
#=╠═╡
draw(A; isymbols = isymbols, osymbols = osymbols)
  ╠═╡ =#

# ╔═╡ 5a23fd29-06eb-4337-ac8f-dc005db7102f
# ╠═╡ disabled = true
#=╠═╡
draw(
	closure(A); 
	isymbols = isymbols,
	osymbols = osymbols
)
  ╠═╡ =#

# ╔═╡ 1853a04e-ab13-4903-a8e5-ec840909d4fc
#=╠═╡
B = TensorFST(
	[
		(src = 1, isym = 2, osym = 3, weight = one(S), dest = 2),
		(src = 2, isym = 3, osym = 4, weight = one(S), dest = 3),
		(src = 3, isym = 1, osym = 2, weight = one(S), dest = 1),
	],
	[1 => one(S)],
	[3 => one(S)]
)
  ╠═╡ =#

# ╔═╡ e9365726-92bb-40e8-bce7-9bdbd73466db
#=╠═╡
draw(B; isymbols, osymbols)
  ╠═╡ =#

# ╔═╡ 5de71452-3697-4b88-ab98-729c409fa1e7
#=╠═╡
union(A, B)
  ╠═╡ =#

# ╔═╡ 0bd2b6e0-9f4a-4fd7-be09-4b1d9e6149a3
#=╠═╡
draw(union(A, B))
  ╠═╡ =#

# ╔═╡ c3bc6446-2ac4-4954-aebb-acd53e7c78a1
#=╠═╡
draw(cat(A, B))
  ╠═╡ =#

# ╔═╡ 115093d8-767d-4580-ab1a-657b72a2b293
#=╠═╡
draw(B; isymbols = isymbols, osymbols = osymbols)
  ╠═╡ =#

# ╔═╡ d720abcc-b145-4790-b964-1cbea4bca766
# ╠═╡ disabled = true
#=╠═╡
draw(
	union(A, B); 
	isymbols = isymbols,
	osymbols = osymbols
)
  ╠═╡ =#

# ╔═╡ a528f8af-df53-4c31-af8b-6040ce4692b9
# ╠═╡ disabled = true
#=╠═╡
draw(
	cat(A, B); 
	isymbols = isymbols,
	osymbols = osymbols
)
  ╠═╡ =#

# ╔═╡ 0d26c190-a2c2-4663-8288-56b66b1a2284
#=╠═╡
C = TensorFST(
	[
		(src = 1, isym = 2, osym = 2, weight = one(S), dest = 2),
		(src = 1, isym = 3, osym = 3, weight = one(S), dest = 2),
		(src = 2, isym = 4, osym = 4, weight = one(S), dest = 3),
	],
	[1 => one(S)],
	[3 => one(S)]
)
  ╠═╡ =#

# ╔═╡ a9338032-0584-4a58-a253-e0811e390035
#=╠═╡
draw(C)
  ╠═╡ =#

# ╔═╡ 9ff69daf-a84b-49bc-8cb6-526ae7caf3d9
#=╠═╡
weight(C)
  ╠═╡ =#

# ╔═╡ ff5057ef-c586-4d90-b493-527cf13c9700
# ╠═╡ disabled = true
#=╠═╡
draw(
	B^*; 
	isymbols = isymbols,
	osymbols = osymbols
)
  ╠═╡ =#

# ╔═╡ e3a70f1c-839a-493a-9e4b-405332a187f0
# ╠═╡ disabled = true
#=╠═╡
val(zero(FSTSemiring{S})) |> draw
  ╠═╡ =#

# ╔═╡ 6339906a-c827-4fc3-bb5a-eef5d36a58c7
# ╠═╡ disabled = true
#=╠═╡
val(one(FSTSemiring{S})) |> draw
  ╠═╡ =#

# ╔═╡ 7f6096e9-bab3-4108-9bd7-edb46fde8ee4
# ╠═╡ disabled = true
#=╠═╡
val(FSTSemiring(A) ⊕ FSTSemiring(B)) |> draw
  ╠═╡ =#

# ╔═╡ 4d193eae-5c15-4846-a9a1-29651d39fd4d
# ╠═╡ disabled = true
#=╠═╡
val(FSTSemiring(A)^*) |> draw
  ╠═╡ =#

# ╔═╡ 6be995d2-717b-4bc9-8b01-702aa0ed81f3
md"""## Example: FST from regex strings

In this example we show how to build an FST accepting the sequences matching a "regex string", e.g. `abc[A-Z][0-9]*`

The syntax of our regex string will be:
* "abc" => accepts the sequence "abc"
* "abc|efg" accepts the sequence "abc" or "efg"
* "abc[123]" accepts the sequence "abc" followed by "1" or "2" or "3"
* "abc[09]" accepts the sequence "abc" followed by any symbols from "0" to "9" 
* "abc*" accepts zero, one or any number of repetition of the string "abc"
* "abc*" accepts  one or any number of repetition of the string "abc"
* "abc(...) accepts "abc" followed the the regex string "...".
"""

# ╔═╡ d5a7837a-6af0-442e-b00f-230361e938ed
md"""
### Implementation

We consider two atomic elements: 
- sequences such as "abcasdf" are modeled as linear FST
- sets such as "[abcd]" which we model as "multiple choice" FST.
"""

# ╔═╡ cd1bcb43-c272-4e21-a76b-5fb0e591864b
# ╠═╡ disabled = true
#=╠═╡
function LinearFST(S::Type{<:Semiring}, seq; symbols::Dict)
    arcs = Arc{S}[]
    for (i, x) in enumerate(seq)
		arc = Arc(
			src = i,
			dest = i+1,
			isym = symbols[x],
			osym = symbols[x], 
			weight=one(S)
		)
        push!(arcs, arc)
    end
    TensorFST(arcs, [1 => one(S)], [length(seq)+1 => one(S)])
end
  ╠═╡ =#

# ╔═╡ 8437e905-014a-43c4-ab30-e2964b15a0e4
# ╠═╡ disabled = true
#=╠═╡
function MultipleChoiceFST(S, seq; symbols)
    arcs = Arc{S}[]
    for (i, x) in enumerate(seq)
		arc = Arc(
			src = 1,
			dest = 2,
			isym = symbols[x],
			osym = symbols[x], 
			weight=one(S)
		)
        push!(arcs, arc)
    end
    TensorFST(arcs, [1 => one(S)], [2 => one(S)])
end
  ╠═╡ =#

# ╔═╡ 01966d51-6344-4ce3-9e37-6005e394afe4
# ╠═╡ disabled = true
#=╠═╡
abc = LinearFST(S, "abc"; symbols = Dict('a' => 2, 'b' => 3, 'c' => 4))
  ╠═╡ =#

# ╔═╡ 7df33e86-a9e9-44ff-ae17-75e0f5fe2e2b
# ╠═╡ disabled = true
#=╠═╡
a_b_c = MultipleChoiceFST(S, "abc"; symbols = Dict('a' => 2, 'b' => 3, 'c' => 4))
  ╠═╡ =#

# ╔═╡ 74b826ba-c0e8-405f-875e-fd16cf0f2f64
# ╠═╡ disabled = true
#=╠═╡
draw(abc)
  ╠═╡ =#

# ╔═╡ 92aad52c-d90f-4b99-bbaf-05b0cd17b35c
# ╠═╡ disabled = true
#=╠═╡
draw(a_b_c)
  ╠═╡ =#

# ╔═╡ 775251ef-9a96-4eea-8aa2-bda85c3dbe79
#=╠═╡
function parseregex_set(S, str, itstate; symbols)
	# Initialize the FST regex as an empty FST.
    regex = zero(FSTSemiring{S})
	
	# Buffer to keep symbols for the union. 
	stack = []
	
	# Consume the next item from the iterable `str`.
	# `next` contains the value and the new iteration state.
    next = isnothing(itstate) ? iterate(str) : iterate(str, itstate)

	# Whether or not the set expression `[...]` is properly closed.
	# For now, we assume it is not.
    valid = false
	
    while !isnothing(next)
		# `c` is the current symbol, `itstate is the current status 
		# of the iterator.
        c, itstate = next
		
        if c ∈ Set("|()[*+")
			# `c` is a special symbol. We don't support them for sets.
            throw(ArgumentError("cannot used one of \"|()[*+\" in sets"))
        end

        if c == ']'
			# We have reached the end of the set definition. We interrupt 
			# the parsing but does not return immediately as there may 
			# be some symbols in the stack.
            valid = true
            break
        elseif c == '-' # Range definition, e.g. `0-9`.

			# Consume the next iterable to get the end of the range.
            next = iterate(str, itstate)

			# Makes sure we have all the information to have the range.
            (isempty(stack) || isnothing(next) || next[1] == ']') && throw(ArgumentError("invalid range"))

			# Get the start value of the range from the stack.
            prevc = pop!(stack)

			# Get the end value of the range.
            nextc, itstate = next

			# Add to the stack all the value in the range (boundaries 
			# included). This will raise an error if `prevc`:`nextc` 
			# does not define a Julia range.
            for c in prevc:nextc
                push!(stack, c)
            end
        else 
			# regular character append to the stack for union
            push!(stack, c)
        end
		
		# Consume the next iterable.
        next = iterate(str, itstate)
    end

	# We have consumed everything but we haven't met the closing `]`.
    ! valid && throw(ArgumentError("malformed expression: missing ']'"))

	# If it remains symbols in the stack add them to the regex.
    if ! isempty(stack)
		fst = MultipleChoiceFST(S, stack; symbols)
        regex = regex ⊕ FSTSemiring(fst)
    end

	# Return the regex and the current status of the iterator.
    regex, itstate
end

  ╠═╡ =#

# ╔═╡ 08dc8a44-a107-4c98-8cb5-123f18900171
#=╠═╡
function parseregex(S, str, itstate = nothing, bcounts = 0; symbols)
	# Initialize the FST regex as a simple FST just accepting ϵ
	# with weight 1.
    regex = one(FSTSemiring{S})

	# Buffer to keep symbols to concatenate together. 
	stack = []

	# Consume the next item from the iterable `str`.
	# `next` contains the value and the new iteration state.
    next = isnothing(itstate) ? iterate(str) : iterate(str, itstate)

	# Iterate until all the iterable has been consumed.
    while ! isnothing(next)
		# `c` is the current symbol, `itstate is the current status 
		# of the iterator.
        c, itstate = next

        if c ∈ Set("|()[]*+")
			# `c` is a special symbol. 
			
			# We update the regex by concatenating it with 
			# all the symbols consumed (put in `stack`).
            if !isempty(stack)
				fst = LinearFST(S, stack; symbols)
                regex = regex ⊗ FSTSemiring(fst)
                empty!(stack)
            end

            if c == '|' 
				# Union, split the regex into two alternative regex. 
				# The first part is already parsed. 
				
				# Parse the second part of the regex
                subregex, itstate = parseregex(S, str, itstate, bcounts; symbols)

				# Build the final regex.
                regex = regex ⊕ subregex

				# Exit parsing.
                return regex, itstate
            elseif c == '['
				# Set, parse the inside of `[...]` but using union between
				# regex atoms rather than concatenation. `parseregex_set`
				# returns once it encounters the closing `]`.
                subregex, itstate = parseregex_set(S, str, itstate; symbols)
				# Concatenate the current regex with the set.
                regex = regex ⊗ subregex
            elseif c == '('
				# Parse the inside of `(...)` .
                subregex, itstate = parseregex(S, str, itstate, bcounts + 1; symbols)
				
				# Concatenate the current regex with the sub-regex.
                regex = regex ⊗ subregex
            elseif c == ')'
                bcounts == 0 && throw(ArgumentError("malformed expression: invalid number of parentheses"))
                return regex, itstate
            elseif c == '+'
				# Apply a closure (+) to the current regex.
                regex = regex ⊗ regex^*
            else # c == '*'
				# Apply a closure to the current regex.
                regex = regex^*
            end
        else 
			# `c` is a standard symbol, append to the stack for
			# concatenation.
            push!(stack, c)
        end

		# Consume the next iterable.
        next = iterate(str, itstate)
    end

	# Safety check: make sure that all parentheses have been closed.
    bcounts > 0 && throw(ArgumentError("malformed expression: invalid number of parentheses"))

	# If it remains some symbols on the stack, just concatenate them
	# to the regex.
    if ! isempty(stack)
		fst = LinearFST(S, stack; symbols)
		regex = regex ⊗ FSTSemiring(fst)
    end

	# Return the regex and the current status of the iterator.
    regex, itstate
end

  ╠═╡ =#

# ╔═╡ f33d83a4-f719-4274-8a81-3e712c4d81b6
md"""
Our main function will the `compile` function which takes as arguments a semiring `S`, a string to parse `str` and the symbols mapping (mandatory).
"""

# ╔═╡ 4c8892a8-2aed-43a0-b415-9bff49953799
#=╠═╡
function compile(S, str; symbols) 
	# Symbols is id to sym, we need it in the other direction. 
	# Create the reverse mapping before the parsing.
	rev_symbols = Dict(v => k for (k, v) in symbols)
	
	regex, _ = parseregex(S, str, nothing; symbols = rev_symbols)
	val(regex)
end
  ╠═╡ =#

# ╔═╡ d4b0c558-dac9-4080-b6fe-3982ddfbbbda
md"""#### Demonstration"""

# ╔═╡ 82d1628e-3c8b-4a4d-8f50-3f6532320d9c
# ╠═╡ disabled = true
#=╠═╡
function make_alphanumeric() 
	symbols = Dict(1 => 'ϵ')
	merge!(symbols, Dict(i+length(symbols) => c for (i, c) in enumerate('a':'z')))
	merge!(symbols, Dict(i+length(symbols) => c for (i, c) in enumerate('A':'Z')))
	merge!(symbols, Dict(i+length(symbols) => c for (i, c) in enumerate('0':'9')))
end
  ╠═╡ =#

# ╔═╡ 2b0ec3c7-7553-4ac3-9737-f4ad8a23300d
# ╠═╡ disabled = true
#=╠═╡
function make_emoji() 
	symbols = Dict(1 => 'ϵ')
	merge!(symbols, Dict(i+length(symbols) => c for (i, c) in enumerate('😀':'😇')))
	merge!(symbols, Dict(i+length(symbols) => c for (i, c) in enumerate('😹':'🙁')))
end
  ╠═╡ =#

# ╔═╡ b713f3a2-5d47-482e-a6f1-34c067447dbd
# ╠═╡ disabled = true
#=╠═╡
alphanumeric = make_alphanumeric()
  ╠═╡ =#

# ╔═╡ 0a12b190-0217-40ab-b63a-369fc7003268
# ╠═╡ disabled = true
#=╠═╡
emoji = make_emoji()
  ╠═╡ =#

# ╔═╡ 35ade6c7-6d55-47eb-b89e-32745032778a
# ╠═╡ disabled = true
#=╠═╡
draw(
	compile(S, "a[1-9]b(efg*)"; symbols = alphanumeric);
	isymbols = alphanumeric,
	osymbols = alphanumeric
)
  ╠═╡ =#

# ╔═╡ d6627625-606e-4302-bb01-76e8eb7a985d
# ╠═╡ disabled = true
#=╠═╡
draw(
	compile(S, "😺😃😆😂|😿(🙁+)"; symbols = emoji);
	isymbols = emoji,
	osymbols = emoji
)
  ╠═╡ =#

# ╔═╡ Cell order:
# ╟─a4127267-40fd-4550-9446-6d82b0ae8373
# ╟─701e9298-89fb-4082-8268-a57bd4cda574
# ╠═c893a80a-83cd-11ee-1a82-d7c85de7ab7a
# ╟─f781cf77-3332-48f8-b2dc-4d4880c81a08
# ╠═4684d1ff-7a80-4e8e-9f48-f3ef5b11cb5a
# ╠═ebe54b31-daa5-4970-9a25-4146d9f101d8
# ╠═1853a04e-ab13-4903-a8e5-ec840909d4fc
# ╠═0d26c190-a2c2-4663-8288-56b66b1a2284
# ╠═a9338032-0584-4a58-a253-e0811e390035
# ╠═9ff69daf-a84b-49bc-8cb6-526ae7caf3d9
# ╟─b4279331-1f8c-49b7-9bc4-b46f912ace46
# ╟─d62946ec-112e-40c4-8b8c-24dd6f1c5103
# ╠═27d2c155-518c-49fc-b273-0853f7a04c75
# ╟─f779c388-223c-4299-9523-3f3b3ba21769
# ╠═41df922b-543f-433e-9e30-b6863ca00366
# ╠═e9365726-92bb-40e8-bce7-9bdbd73466db
# ╠═5de71452-3697-4b88-ab98-729c409fa1e7
# ╠═0bd2b6e0-9f4a-4fd7-be09-4b1d9e6149a3
# ╠═c3bc6446-2ac4-4954-aebb-acd53e7c78a1
# ╠═2a48bcea-91cf-4c85-8362-c25eaefa965f
# ╟─6f79ca2f-9c47-49a6-823f-426f4abb6999
# ╠═72d16117-5949-4260-9549-8e5773c37148
# ╠═240ee7f9-c1aa-4d46-804a-55b30fcd2249
# ╟─d048d3ee-98e0-4fbc-a6cb-c34adc8e9025
# ╠═7e2dec0f-a92a-4b39-96dd-94d6f61519d8
# ╠═115093d8-767d-4580-ab1a-657b72a2b293
# ╟─4224ebb5-9a46-415e-8a3f-0231ff2b1704
# ╠═d720abcc-b145-4790-b964-1cbea4bca766
# ╠═a528f8af-df53-4c31-af8b-6040ce4692b9
# ╠═5a23fd29-06eb-4337-ac8f-dc005db7102f
# ╠═ff5057ef-c586-4d90-b493-527cf13c9700
# ╟─9d584247-568b-4378-8d1e-8a8ca8e113ed
# ╠═17483a0e-9633-46b3-aed0-8ba955eeb515
# ╠═e3a70f1c-839a-493a-9e4b-405332a187f0
# ╠═6339906a-c827-4fc3-bb5a-eef5d36a58c7
# ╠═7f6096e9-bab3-4108-9bd7-edb46fde8ee4
# ╠═4d193eae-5c15-4846-a9a1-29651d39fd4d
# ╟─6be995d2-717b-4bc9-8b01-702aa0ed81f3
# ╟─d5a7837a-6af0-442e-b00f-230361e938ed
# ╠═cd1bcb43-c272-4e21-a76b-5fb0e591864b
# ╠═8437e905-014a-43c4-ab30-e2964b15a0e4
# ╠═01966d51-6344-4ce3-9e37-6005e394afe4
# ╠═7df33e86-a9e9-44ff-ae17-75e0f5fe2e2b
# ╠═74b826ba-c0e8-405f-875e-fd16cf0f2f64
# ╠═92aad52c-d90f-4b99-bbaf-05b0cd17b35c
# ╟─775251ef-9a96-4eea-8aa2-bda85c3dbe79
# ╟─08dc8a44-a107-4c98-8cb5-123f18900171
# ╟─f33d83a4-f719-4274-8a81-3e712c4d81b6
# ╠═4c8892a8-2aed-43a0-b415-9bff49953799
# ╟─d4b0c558-dac9-4080-b6fe-3982ddfbbbda
# ╠═82d1628e-3c8b-4a4d-8f50-3f6532320d9c
# ╠═2b0ec3c7-7553-4ac3-9737-f4ad8a23300d
# ╠═b713f3a2-5d47-482e-a6f1-34c067447dbd
# ╠═0a12b190-0217-40ab-b63a-369fc7003268
# ╠═35ade6c7-6d55-47eb-b89e-32745032778a
# ╠═d6627625-606e-4302-bb01-76e8eb7a985d
