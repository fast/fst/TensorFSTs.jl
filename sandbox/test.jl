### A Pluto.jl notebook ###
# v0.19.27

using Markdown
using InteractiveUtils

# ╔═╡ a433081c-4d58-11ee-2591-5972f009ea6d
begin
	#push!(LOAD_PATH, "../")

	using Pkg
	Pkg.activate("..")
	
	using Revise
	using LinearAlgebra
	using TensorFSTs
	using Semirings
end

# ╔═╡ fcc7ff6f-7a24-4ef0-b47f-3c794558d641
symbols = getsymboltable("latin.syms")

# ╔═╡ cdaa0497-e33a-45f9-9c86-24920a0f68db
S = LogSemiring{Float64,1}

# ╔═╡ bce9ca01-ed89-45f8-bd03-fcd8b0b45283
textfst1 = """
1 2 2 1 2
2 3 3 11 3 
1 3 2 12 4
3 4 3 13 5
3 23
4
"""

# ╔═╡ af0cba32-a718-45b4-9c82-89195d017190
X = compile(textfst1; semiring = S)

# ╔═╡ 3e6645b0-43ab-4936-beb3-3898248a2c38
getM(X)

# ╔═╡ 542851a1-bcf4-4236-b983-f93000288a87
draw(X; isymbols = symbols, osymbols = symbols)

# ╔═╡ e6c19def-7dea-4b50-8417-40b86c143383
shortestdistance(X)

# ╔═╡ 211012f4-551c-4364-a7a5-a3edb73f8502
summary(X)

# ╔═╡ e7302281-c2fa-4aa7-af4c-2a4f9fb7f4d5
X ⊗ X

# ╔═╡ c6595865-0db2-4c48-a43c-1cf7e493c44c
draw(X ⊗ X; isymbols = symbols, osymbols = symbols)

# ╔═╡ 6fbe5989-97a3-4574-9ced-cb8fbd4f7a48
draw(X ⊕ X; isymbols = symbols, osymbols = symbols)

# ╔═╡ 7ed96a14-ce68-46b1-a196-e30a356a89a9
draw(X; symbols)

# ╔═╡ b92032d6-9528-46d6-a148-b2d5d77c41a2
draw(X^*; symbols)

# ╔═╡ 30e02e1a-3d78-4ae6-b9ab-971843ea4f7a
T = typeof(X)

# ╔═╡ 251a3c14-46bc-4652-a6ac-b2726a4044cd
zero(TensorFST{S}) ⊕ X |> draw

# ╔═╡ Cell order:
# ╠═a433081c-4d58-11ee-2591-5972f009ea6d
# ╠═fcc7ff6f-7a24-4ef0-b47f-3c794558d641
# ╠═cdaa0497-e33a-45f9-9c86-24920a0f68db
# ╠═bce9ca01-ed89-45f8-bd03-fcd8b0b45283
# ╠═af0cba32-a718-45b4-9c82-89195d017190
# ╠═3e6645b0-43ab-4936-beb3-3898248a2c38
# ╠═542851a1-bcf4-4236-b983-f93000288a87
# ╠═e6c19def-7dea-4b50-8417-40b86c143383
# ╠═211012f4-551c-4364-a7a5-a3edb73f8502
# ╠═e7302281-c2fa-4aa7-af4c-2a4f9fb7f4d5
# ╠═c6595865-0db2-4c48-a43c-1cf7e493c44c
# ╠═6fbe5989-97a3-4574-9ced-cb8fbd4f7a48
# ╠═7ed96a14-ce68-46b1-a196-e30a356a89a9
# ╠═b92032d6-9528-46d6-a148-b2d5d77c41a2
# ╠═30e02e1a-3d78-4ae6-b9ab-971843ea4f7a
# ╠═251a3c14-46bc-4652-a6ac-b2726a4044cd
