### A Pluto.jl notebook ###
# v0.19.38

using Markdown
using InteractiveUtils

# ╔═╡ ae006bf0-bf52-11ee-0f13-eb0d6604b7d8
begin
	using Pkg
	Pkg.activate("..")

	using Revise
	using Semirings
	using SumSparseTensors
	using TensorFSTs
end

# ╔═╡ 73da9383-7154-47d1-84e3-926ba5890a69
Pkg.status()

# ╔═╡ 33a1f878-32a8-400c-b402-b079b4493162
S = ProbSemiring{Float32}

# ╔═╡ a8835591-fa75-4539-9a84-219ba11dea04
A = TensorFST(
	[
		(src = (1,), isym = 2, osym = 2, weight = one(S), dest = (2,)),
		(src = (2,), isym = 3, osym = 3, weight = one(S), dest = (3,)),
		(src = (1,), isym = 2, osym = 3, weight = one(S), dest = (3,))
	],
	[(1,) => one(S)],
	[(3,) => one(S)]
)

# ╔═╡ bda00782-55f9-4ff3-929c-71a8ba26074f
draw(A)

# ╔═╡ 75996b0a-1492-4bde-8d5e-1b8eb2ee1d32
B = TensorFST(
	[
		(src = (1,), isym = 2, osym = 2, weight = one(S), dest = (2,))
	],
	[(1,) => one(S)],
	[(2,) => one(S)]
)

# ╔═╡ edacfd6e-ad4b-4bc4-9436-95111bc02ed0
draw(B)

# ╔═╡ 4f8bc52f-a67b-422f-ae9c-bea5abc1c301
C = TensorFST(
	[
		(src = (1, 2), isym = 2, osym = 2, weight = one(S), dest = (2, 1)),
		(src = (2, 1), isym = 3, osym = 3, weight = one(S), dest = (3, 3)),
		(src = (1, 2), isym = 2, osym = 3, weight = one(S), dest = (3, 3)),
	],
	[(1, 2) => one(S)],
	[(3, 3) => one(S)]
)

# ╔═╡ a279859b-8912-433f-be63-8bbd3405c603
draw(C)

# ╔═╡ d4a1c632-066b-4aae-a45f-b7be467ea99a
D = TensorFST(
	[
		(src = (1, 2), isym = 2, osym = 2, weight = one(S), dest = (2, 1)),
	],
	[(1, 2) => one(S)],
	[(2, 1) => one(S)]
)

# ╔═╡ f9012cd4-9416-44f7-aa4a-845cf3d17e24
draw(D)

# ╔═╡ 3ea4db77-15c3-49c8-bfa7-9f8b49ba29f8
(A = summary(A), B = summary(B))

# ╔═╡ 1ecfbac9-d522-459c-8900-cd13bd652a98
(C = summary(C), D = summary(D))

# ╔═╡ 6871335f-150f-4f64-b3d2-b0f7db0cad3e
union(A, B) |> draw

# ╔═╡ f58bbed2-df28-4e96-96e7-82ffd993488a
union(C, D) |> draw

# ╔═╡ 6e73f50a-fff8-417c-9b39-93491cc084b4
cat(A, B) |> draw

# ╔═╡ c5d3f5fc-b0eb-4293-91f4-5fb122b926f8
cat(C, D) |> draw

# ╔═╡ 1a1a2ed5-32ea-4af9-9be8-e3ab835c5c50
closure(A) |> draw

# ╔═╡ 8325864c-dff0-4cdd-90ba-6aa3f64457a0
closure(C) |> draw

# ╔═╡ 19dbdec1-0f10-4810-8fa4-2bcd70c9fd32
weight(A), weight(C)

# ╔═╡ c02cf76b-7873-4818-88fd-f042cd2313b8
shortestdistance(A)

# ╔═╡ 5aca545a-2f29-4437-8c89-4358ed39d6d5
shortestdistance(C)

# ╔═╡ 08d22cd0-5d6e-4c06-b1af-bf3c526ab732
md"""
## Composition
"""

# ╔═╡ 53ac37da-a957-46d1-9d83-535589a188e3
symbols = Dict(
	1 => "ϵ",
	2 => "a",
	3 => "b"
)

# ╔═╡ 217f1776-34da-42e5-9e82-a9d81aefdc32
T1 = TensorFST(
	[
		(src = (1,), isym = 2, osym = 3, weight = S(0.1), dest = (2,)),
		(src = (2,), isym = 2, osym = 3, weight = S(0.2), dest = (1,)),
		(src = (2,), isym = 3, osym = 3, weight = S(0.3), dest = (3,)),
		(src = (2,), isym = 3, osym = 3, weight = S(0.4), dest = (4,)),
		(src = (3,), isym = 2, osym = 3, weight = S(0.5), dest = (4,)),
		(src = (4,), isym = 2, osym = 2, weight = S(0.6), dest = (4,))
	],
	[(1,) => one(S)],
	[(4,) => one(S)]
)

# ╔═╡ 5cf908bc-5c77-4bad-830a-4a8575e438a4
draw(T1; symbols = symbols)

# ╔═╡ 5e6d00e0-0279-45fe-904f-4307e6bbe500
T2 = TensorFST(
	[
		(src = (1,), isym = 3, osym = 3, weight = S(0.1), dest = (2,)),
		(src = (2,), isym = 3, osym = 2, weight = S(0.2), dest = (2,)),
		(src = (2,), isym = 2, osym = 3, weight = S(0.3), dest = (3,)),
		(src = (2,), isym = 2, osym = 3, weight = S(0.4), dest = (4,)),
		(src = (3,), isym = 3, osym = 2, weight = S(0.5), dest = (4,))
	],
	[(1,) => one(S)],
	[(4,) => one(S)]
)

# ╔═╡ 69569934-435d-4254-b2b8-67a81eade7bf
draw(T2; symbols = symbols)

# ╔═╡ ad054c95-3dbc-4f2c-af45-9abcf73b1844
draw(T1; symbols), draw(T2; symbols)

# ╔═╡ 7da33db6-80ec-4a7c-bee2-f39432f110a2
draw(compose(T1, sort(T2, inputlabel)); symbols)

# ╔═╡ 28c0cfad-84e5-4d35-88dc-0babd9a09f40
draw(compose(T1, sort(T2, inputlabel); connect = false, flatten = false); symbols)

# ╔═╡ db1b80b6-1c27-4941-b8d8-6cefd2011b9e
draw(
	compose(T1, sort(T2, inputlabel); connect = true); 
	symbols
)

# ╔═╡ f533a7b9-2698-4e43-aac1-3ed7e48a32a1
T3 = compose(T1, sort(T2, inputlabel); connect = true)

# ╔═╡ 13e3f87a-a6ae-435a-ad1b-b0a1e195d7ef
draw(T3; symbols)

# ╔═╡ 3f22e2ab-f05c-4d6e-81e8-0a0aecfa7972
const Ngram2 = NTuple

# ╔═╡ e38d1e93-0abd-430c-8b72-7a009f6602e6
draw(connect(T3))

# ╔═╡ 231c8ded-c0a5-4473-8ec8-4f965ced6acf
weight(T3)

# ╔═╡ 4d5283b4-5217-4359-9de2-3be9159777ff
shortestdistance(C)

# ╔═╡ 711788f6-9f53-4914-a8ed-84f11f3d8d22
connect(T3) 

# ╔═╡ 90e06eb4-4df3-4b64-a20c-f20fef275659
T3.α |> size

# ╔═╡ 16623f2d-d906-4d85-b295-831aa0c8c3a9
draw(T3)

# ╔═╡ a70f77b8-6e85-4bac-9a29-4fa18b5e9895
draw(TensorFSTs.flatten(T3); symbols)

# ╔═╡ 1fb1e1d1-09c8-4888-ad23-564fc94555dd
arcs(T3)

# ╔═╡ 21ca1c61-623d-4011-8a51-ae0732cfed72
states(T3)

# ╔═╡ 1e8fdb9b-5e20-44bf-bac7-f3c264faa118
initweights(T3)

# ╔═╡ c2bab8d0-07d4-436d-a8ca-9df1906107f2
finalweights(T3)

# ╔═╡ Cell order:
# ╠═ae006bf0-bf52-11ee-0f13-eb0d6604b7d8
# ╠═73da9383-7154-47d1-84e3-926ba5890a69
# ╠═33a1f878-32a8-400c-b402-b079b4493162
# ╠═a8835591-fa75-4539-9a84-219ba11dea04
# ╠═bda00782-55f9-4ff3-929c-71a8ba26074f
# ╠═75996b0a-1492-4bde-8d5e-1b8eb2ee1d32
# ╠═edacfd6e-ad4b-4bc4-9436-95111bc02ed0
# ╠═4f8bc52f-a67b-422f-ae9c-bea5abc1c301
# ╠═a279859b-8912-433f-be63-8bbd3405c603
# ╠═d4a1c632-066b-4aae-a45f-b7be467ea99a
# ╠═f9012cd4-9416-44f7-aa4a-845cf3d17e24
# ╠═3ea4db77-15c3-49c8-bfa7-9f8b49ba29f8
# ╠═1ecfbac9-d522-459c-8900-cd13bd652a98
# ╠═6871335f-150f-4f64-b3d2-b0f7db0cad3e
# ╠═f58bbed2-df28-4e96-96e7-82ffd993488a
# ╠═6e73f50a-fff8-417c-9b39-93491cc084b4
# ╠═c5d3f5fc-b0eb-4293-91f4-5fb122b926f8
# ╠═1a1a2ed5-32ea-4af9-9be8-e3ab835c5c50
# ╠═8325864c-dff0-4cdd-90ba-6aa3f64457a0
# ╠═19dbdec1-0f10-4810-8fa4-2bcd70c9fd32
# ╠═c02cf76b-7873-4818-88fd-f042cd2313b8
# ╠═5aca545a-2f29-4437-8c89-4358ed39d6d5
# ╠═08d22cd0-5d6e-4c06-b1af-bf3c526ab732
# ╠═53ac37da-a957-46d1-9d83-535589a188e3
# ╠═217f1776-34da-42e5-9e82-a9d81aefdc32
# ╠═5cf908bc-5c77-4bad-830a-4a8575e438a4
# ╠═5e6d00e0-0279-45fe-904f-4307e6bbe500
# ╠═69569934-435d-4254-b2b8-67a81eade7bf
# ╠═ad054c95-3dbc-4f2c-af45-9abcf73b1844
# ╠═7da33db6-80ec-4a7c-bee2-f39432f110a2
# ╠═28c0cfad-84e5-4d35-88dc-0babd9a09f40
# ╠═db1b80b6-1c27-4941-b8d8-6cefd2011b9e
# ╠═f533a7b9-2698-4e43-aac1-3ed7e48a32a1
# ╠═13e3f87a-a6ae-435a-ad1b-b0a1e195d7ef
# ╠═3f22e2ab-f05c-4d6e-81e8-0a0aecfa7972
# ╠═e38d1e93-0abd-430c-8b72-7a009f6602e6
# ╠═231c8ded-c0a5-4473-8ec8-4f965ced6acf
# ╠═4d5283b4-5217-4359-9de2-3be9159777ff
# ╠═711788f6-9f53-4914-a8ed-84f11f3d8d22
# ╠═90e06eb4-4df3-4b64-a20c-f20fef275659
# ╠═16623f2d-d906-4d85-b295-831aa0c8c3a9
# ╠═a70f77b8-6e85-4bac-9a29-4fa18b5e9895
# ╠═1fb1e1d1-09c8-4888-ad23-564fc94555dd
# ╠═21ca1c61-623d-4011-8a51-ae0732cfed72
# ╠═1e8fdb9b-5e20-44bf-bac7-f3c264faa118
# ╠═c2bab8d0-07d4-436d-a8ca-9df1906107f2
