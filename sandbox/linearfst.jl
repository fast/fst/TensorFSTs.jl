### A Pluto.jl notebook ###
# v0.19.38

using Markdown
using InteractiveUtils

# ╔═╡ 02333f44-c66d-11ee-311f-ebc7400246f8
begin
	using Pkg
	Pkg.activate("..")

	using BenchmarkTools
	using Revise
	using Semirings
	using TensorFSTs
	using TensorFSTs.LinearFSTs
end

# ╔═╡ 4a7189bc-c029-4990-9459-441a03bd2b44
S = ProbSemiring{Float32}

# ╔═╡ 5f9e5549-3416-4059-b165-d3c18cb5baf2
X = S.(ones(2, 3))

# ╔═╡ be351f5c-ee47-42da-8b1d-07bd85615db3
isymbolmap = [2, 3, 4, 5, 6]

# ╔═╡ 59b89ee9-db0f-42ae-98e2-68c6c75bbeff
isymbolmap[1] 

# ╔═╡ bb40fec3-ebb1-41c5-b530-6ea7fe121f0f
osymbolmap = [6, 5, 4, 3, 2, 1]

# ╔═╡ 0b486637-d9da-416c-84a8-40af6f82c261
linearfst(X, isymbolmap) |> draw

# ╔═╡ 962e091b-2c0d-4673-9f5e-98c58627744b
linearfst(X, isymbolmap) |> arcs

# ╔═╡ 2e373e93-711a-41bd-8a66-629b1654eee8
linearfst(X, isymbolmap, osymbolmap) |> draw

# ╔═╡ ab28a16c-1339-4c9e-9364-6b3b70820b4c
linearfst(X, isymbolmap; αval = S.(rand(4))) |> draw

# ╔═╡ 5e1f4bcb-2236-4718-beab-121ab395f02c
linearfst(X, isymbolmap; αval = S.(rand(4)), ωval = S.(rand(4))) |> draw

# ╔═╡ 5b0e29e8-d578-438d-afe0-14c20a7338f8
y = S.(randn(3))

# ╔═╡ ab7cbc27-0697-45b7-afef-d3589b63f31b
linearfst(y, isymbolmap, osymbolmap) |> draw

# ╔═╡ c66897ea-f884-4624-b426-4b65faab11e3
linearfst(y, isymbolmap, osymbolmap; ωval = S.(randn(4))) |> draw

# ╔═╡ a0f138fd-1cbf-4ff3-97eb-2d59395e18ad
A = linearfst(y, isymbolmap, osymbolmap; ωval = S.(randn(4))) 

# ╔═╡ 7a45ac56-bcab-4945-9006-77efeda86f20
arcs(A)

# ╔═╡ 1f1eab5c-db4a-487a-8de1-c03678c8cc92
initweights(A)

# ╔═╡ 15e3b621-071f-442e-b5df-5d47fe82444d
finalweights(A)

# ╔═╡ Cell order:
# ╠═02333f44-c66d-11ee-311f-ebc7400246f8
# ╠═4a7189bc-c029-4990-9459-441a03bd2b44
# ╠═5f9e5549-3416-4059-b165-d3c18cb5baf2
# ╠═be351f5c-ee47-42da-8b1d-07bd85615db3
# ╠═59b89ee9-db0f-42ae-98e2-68c6c75bbeff
# ╠═bb40fec3-ebb1-41c5-b530-6ea7fe121f0f
# ╠═0b486637-d9da-416c-84a8-40af6f82c261
# ╠═962e091b-2c0d-4673-9f5e-98c58627744b
# ╠═2e373e93-711a-41bd-8a66-629b1654eee8
# ╠═ab28a16c-1339-4c9e-9364-6b3b70820b4c
# ╠═5e1f4bcb-2236-4718-beab-121ab395f02c
# ╠═5b0e29e8-d578-438d-afe0-14c20a7338f8
# ╠═ab7cbc27-0697-45b7-afef-d3589b63f31b
# ╠═c66897ea-f884-4624-b426-4b65faab11e3
# ╠═a0f138fd-1cbf-4ff3-97eb-2d59395e18ad
# ╠═7a45ac56-bcab-4945-9006-77efeda86f20
# ╠═1f1eab5c-db4a-487a-8de1-c03678c8cc92
# ╠═15e3b621-071f-442e-b5df-5d47fe82444d
