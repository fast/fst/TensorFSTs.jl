### A Pluto.jl notebook ###
# v0.19.27

using Markdown
using InteractiveUtils

# ╔═╡ 5b654e10-5302-11ee-0e60-2ddbe4d1129d
begin
	using Pkg
	Pkg.activate("..")
	
	using Revise
	using LinearAlgebra
	using TensorFSTs
	using Semirings
	using Unicode
end

# ╔═╡ 1d4f05f7-1ae7-4190-baf1-c49640741d11
compile("a[0-9]+(uv*)|xyz"; format = :regex) |> draw

# ╔═╡ 4ba82ebc-e865-4454-bea2-b9288578fb18
compile(
	"""
	1 1 1 1 233
	1 2 2 2 123
	"""; 
) |> draw

# ╔═╡ Cell order:
# ╠═5b654e10-5302-11ee-0e60-2ddbe4d1129d
# ╠═1d4f05f7-1ae7-4190-baf1-c49640741d11
# ╠═4ba82ebc-e865-4454-bea2-b9288578fb18
