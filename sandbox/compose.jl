### A Pluto.jl notebook ###
# v0.19.38

using Markdown
using InteractiveUtils

# ╔═╡ dbc86d06-caaa-11ee-394e-75ba75d86a03
begin
	using Pkg
	Pkg.activate("..")

	using Revise
	using Semirings
	using TensorFSTs
	using SumSparseTensors
end

# ╔═╡ c5467f43-c01d-4a55-8eda-a0730df877a1
Pkg.update()

# ╔═╡ f100e3e1-121f-498c-9375-b1d8e19202ac
S = ProbSemiring{Float32}

# ╔═╡ e5b6cf93-021f-4ae4-b69c-cc1c697c4e5c
_A = TensorFST(
        [
            (src = (1,), isym = 2, osym = 3, weight = one(S), dest = (2,)),
            (src = (2,), isym = 2, osym = 3, weight = one(S), dest = (1,)),
            (src = (2,), isym = 3, osym = 3, weight = one(S), dest = (3,)),
            (src = (2,), isym = 3, osym = 3, weight = one(S), dest = (4,)),
            (src = (3,), isym = 2, osym = 3, weight = one(S), dest = (4,)),
            (src = (4,), isym = 2, osym = 2, weight = one(S), dest = (4,))
        ],
        [(1,) => one(S)],
        [(4,) => one(S)]
    )

# ╔═╡ 28866107-c3bf-43cb-9715-cea41afd4bd6
_B = TensorFST(
        [
            (src = (1,), isym = 3, osym = 3, weight = one(S), dest = (2,)),
            (src = (2,), isym = 3, osym = 2, weight = one(S), dest = (2,)),
            (src = (2,), isym = 2, osym = 3, weight = one(S), dest = (3,)),
            (src = (2,), isym = 2, osym = 3, weight = one(S), dest = (4,)),
            (src = (3,), isym = 3, osym = 2, weight = one(S), dest = (4,))
        ],
        [(1,) => one(S)],
        [(4,) => one(S)]
    )


# ╔═╡ c4f94de1-3735-4ee1-bdf8-182d2f133bd0


# ╔═╡ 00c673b4-82ea-4ad3-826d-956f2aea3e1b
_C = TensorFST(
	[
		(src = (1,), isym = 2, osym = 3, weight = one(S), dest = (3,)),
		(src = (2,), isym = 2, osym = 2, weight = one(S), dest = (3,)),
		(src = (3,), isym = 2, osym = 2, weight = one(S), dest = (2,)),
		(src = (3,), isym = 3, osym = 2, weight = one(S), dest = (4,)),
		(src = (3,), isym = 3, osym = 2, weight = one(S), dest = (5,)),
		(src = (4,), isym = 2, osym = 2, weight = one(S), dest = (5,)),
		(src = (5,), isym = 2, osym = 3, weight = one(S), dest = (6,))
	],
	[(1,) => one(S)],
	[(6,) => one(S)]
) 

# ╔═╡ 09866278-d14c-46af-9d3c-64af41771ecd
draw(_C; symbols = Dict(2 => "a", 3 => "b"))

# ╔═╡ e21d4349-157f-41d3-93f7-8fbe715e6e11
draw(compose(_A, sort(_B, inputlabel); connect = true); symbols = Dict(2 => "a", 3 => "b"))

# ╔═╡ cbb78d26-f7c0-4135-8a3e-ea332714309a
syms = SymbolTable(["a", "b", "c", "d", "e"])

# ╔═╡ b685df86-55c7-4b07-9ed5-ab93bb49999d
A = TensorFST(
	[
		(src = 1, isym = syms["a"], osym = syms["a"], weight = one(S), dest = 2),
		(src = 2, isym = syms["b"], osym = syms["ϵ"], weight = one(S), dest = 3),
		(src = 3, isym = syms["c"], osym = syms["ϵ"], weight = one(S), dest = 4),
		(src = 4, isym = syms["d"], osym = syms["d"], weight = one(S), dest = 5)
	],
	[1 => one(S)],
	[5 => one(S)],
	Σrange = 0:5,
	Ωrange = 0:5
)

# ╔═╡ eca237f7-8922-4899-a103-12fd691054d1
B = TensorFST(
	[
		(src = 1, isym = syms["a"], osym = syms["d"], weight = one(S), dest = 2),
		(src = 2, isym = syms["ϵ"], osym = syms["e"], weight = one(S), dest = 3),
		(src = 3, isym = syms["d"], osym = syms["a"], weight = one(S), dest = 4)
	],
	[1 => one(S)],
	[4 => one(S)],
	Σrange = 0:5,
	Ωrange = 0:5
)

# ╔═╡ 4dc856a0-affe-4536-981c-676a09a7f814
(A = summary(A), B = summary(B))

# ╔═╡ 654387c9-6992-4ec3-8b04-847c0390d347
states(A)

# ╔═╡ 911d33ca-81e2-467e-afcf-fe7c8e5aee41
states(B)

# ╔═╡ 779208f1-97b6-4986-9870-4e1059ed1707
draw(A; symbols = syms)

# ╔═╡ 47ffbbd6-9e2f-42ef-a5a0-de4b9a1118a4
draw(B; symbols = syms)

# ╔═╡ 9c820c8d-5f44-49b3-bced-24e1c83611b7
compose(A, sort(B, inputlabel); connect k= false) |> draw

# ╔═╡ ec7667a7-5681-40e4-817e-89b39e3d7ac0


# ╔═╡ f03827c2-5f7b-4fdb-a7a3-2f214e080ccb
compose(A, sort(B, inputlabel); connect= true)

# ╔═╡ c3df3ec1-8665-43e2-b221-d08ca0555dac
compose(A, sort(B, inputlabel); connect = true) 

# ╔═╡ a1329d23-ce33-4abb-8b6a-2b3cfbf18cb5
draw(compose(A, sort(B, inputlabel); connect = true); symbols = syms)

# ╔═╡ b1e7319f-bdb9-4cae-ad51-0155f40bde37
AF, Bl = compose(A, sort(B, inputlabel); connect= true)

# ╔═╡ a66237c4-624f-4da2-9b98-519de0bcdb08
TP = contract(tensorproduct(AF.M, Bl.M), AF.dims.osym, Bl.dims.isym)

# ╔═╡ 0fb07957-9f43-4433-ae42-47358eb21820
findnz(TP.ΣA)

# ╔═╡ d35a4773-2d9c-48b7-a4ec-837073b47638
begin
	struct MyCoo{T<:NTuple{4,Int}} <: AbstractVector{T}
		data::Vector{T}
	end

	Base.size(x::MyCoo) = size(x.data)
	Base.getindex(x::MyCoo, i) = x.data[i]
end

# ╔═╡ 170cb4c7-496a-4d16-9756-947243673485
MyCoo(first(findnz(F.M)))

# ╔═╡ 3cb936ca-8a7e-4b90-830e-6329b65fc37d
first(findnz(F.M))

# ╔═╡ 7849aa16-b366-4383-9276-15d7af1b5aa2
@which copy(F.M)

# ╔═╡ 8faab3ce-361a-445b-8a95-8cbfda635859
nzcoo, nzval = findnz(F.M)

# ╔═╡ 003fbaaa-abd8-46bf-b352-4fa241314cb6
axes(F.M)

# ╔═╡ 9050607b-1db1-43db-931c-22b567626a1d
a, b = compose(A, sort(B, inputlabel); connect = true)

# ╔═╡ 78c54bf9-0ce9-4aa0-b487-e192310b6eec
summary(A), summary(a), summary(b)

# ╔═╡ 75049631-aafd-4775-8216-7dcddea02730
draw(a)

# ╔═╡ 0d15a05e-b3b3-4350-b730-9004734eff15
draw(b)

# ╔═╡ c2ba272f-e1ff-43af-88e3-e6b13c5c92fa
draw(AB; symbols = syms)

# ╔═╡ Cell order:
# ╠═dbc86d06-caaa-11ee-394e-75ba75d86a03
# ╠═c5467f43-c01d-4a55-8eda-a0730df877a1
# ╠═f100e3e1-121f-498c-9375-b1d8e19202ac
# ╠═e5b6cf93-021f-4ae4-b69c-cc1c697c4e5c
# ╠═28866107-c3bf-43cb-9715-cea41afd4bd6
# ╠═c4f94de1-3735-4ee1-bdf8-182d2f133bd0
# ╠═00c673b4-82ea-4ad3-826d-956f2aea3e1b
# ╠═09866278-d14c-46af-9d3c-64af41771ecd
# ╠═e21d4349-157f-41d3-93f7-8fbe715e6e11
# ╠═cbb78d26-f7c0-4135-8a3e-ea332714309a
# ╠═b685df86-55c7-4b07-9ed5-ab93bb49999d
# ╠═eca237f7-8922-4899-a103-12fd691054d1
# ╠═4dc856a0-affe-4536-981c-676a09a7f814
# ╠═654387c9-6992-4ec3-8b04-847c0390d347
# ╠═911d33ca-81e2-467e-afcf-fe7c8e5aee41
# ╠═779208f1-97b6-4986-9870-4e1059ed1707
# ╠═47ffbbd6-9e2f-42ef-a5a0-de4b9a1118a4
# ╠═9c820c8d-5f44-49b3-bced-24e1c83611b7
# ╠═ec7667a7-5681-40e4-817e-89b39e3d7ac0
# ╠═f03827c2-5f7b-4fdb-a7a3-2f214e080ccb
# ╠═c3df3ec1-8665-43e2-b221-d08ca0555dac
# ╠═a1329d23-ce33-4abb-8b6a-2b3cfbf18cb5
# ╠═b1e7319f-bdb9-4cae-ad51-0155f40bde37
# ╠═a66237c4-624f-4da2-9b98-519de0bcdb08
# ╠═0fb07957-9f43-4433-ae42-47358eb21820
# ╠═d35a4773-2d9c-48b7-a4ec-837073b47638
# ╠═170cb4c7-496a-4d16-9756-947243673485
# ╠═3cb936ca-8a7e-4b90-830e-6329b65fc37d
# ╟─7849aa16-b366-4383-9276-15d7af1b5aa2
# ╠═8faab3ce-361a-445b-8a95-8cbfda635859
# ╠═003fbaaa-abd8-46bf-b352-4fa241314cb6
# ╠═9050607b-1db1-43db-931c-22b567626a1d
# ╠═78c54bf9-0ce9-4aa0-b487-e192310b6eec
# ╠═75049631-aafd-4775-8216-7dcddea02730
# ╠═0d15a05e-b3b3-4350-b730-9004734eff15
# ╠═c2ba272f-e1ff-43af-88e3-e6b13c5c92fa
