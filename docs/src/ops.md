# FST Operations

```@meta
CurrentModule = TensorFSTs
```

## Rational Oprations

```@docs
Base.union
Base.cat
closure
```

## Composition

```@docs
compose
```

## Weight of a FST

```@docs
weight
shortestdistance
```

## Optimization

```@docs
connect
flatten
sort
```
