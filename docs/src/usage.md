# Usage

## Defining the semiring
You need to declare the semiring type to use in you FST from the
[Semirings.jl](https://gitlab.lisn.upsaclay.fr/fast/fst/semirings.jl)  package.
For instance, to use the [log semiring](https://en.wikipedia.org/wiki/Log_semiring)
you can set the type
```julia
using Semirings

S = LogSemring{Float64,1}
```

## Creating FSTs
You can create a [`TensorFST`](@ref) object by calling the constructor with a list
of arcs, a list of inital weights and a list of final weights
```julia
A = TensorFST(
	[
		(src = (1,), isym = 2, osym = 2, weight = one(S), dest = (2,)),
		(src = (2,), isym = 3, osym = 3, weight = one(S), dest = (3,)),
		(src = (1,), isym = 2, osym = 3, weight = one(S), dest = (3,))
	],
	[(1,) => one(S)],
	[(3,) => one(S)]
)
```

## Drawing FSTs
It is often convenient to visualize the graphical structure of a FST. You can
draw a FST in a [Pluto](https://plutojl.org/) notebook with the [`draw`](@ref)
function
```julia
isyms = Dict(1 => "ε", 2 => "a", 3 => "b")
osyms = Dict(1 => "ε", 2 => "x", 3 => "y")
draw(A; isymbols = isyms, osymbols = osyms)
```
