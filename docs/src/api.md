# API

```@meta
CurrentModule = TensorFSTs
```

```@docs
TensorFST
arcs
copy
dimensions
draw
finalweights
initweights
numarcs
semiring
states
summary
```
