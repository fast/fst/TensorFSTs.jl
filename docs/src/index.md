# Tensor FSTs

[TensorFSTs.jl](https://gitlab.lisn.upsaclay.fr/fast/fst/TensorFSTs.jl) is a
Julia package for using (weighted) *finite state transducers* (FSTs). It is
largely inspired by [OpenFst](https://www.openfst.org/twiki/bin/view/FST/WebHome)
and provides parallel and differentiable FST algorihtms.

## Install

This package is part of the [FAST](https://gitlab.lisn.upsaclay.fr/fast) tool
collection  and requires the [FAST registry](https://gitlab.lisn.upsaclay.fr/fast/registry)
to be installed. To install the registry type:
```julia
pkg> registry add "https://gitlab.lisn.upsaclay.fr/fast/registry"
```
in the package mode of the Julia REPL. You can then install TensorFSTs.jl
package with the command
```
pkg> add TensorFSTs
```

## Contact

If you have any questions please contact
[Lucas ONDEL YANG](mailto:lucas.ondel-yang@lisn.upsaclay.fr).

