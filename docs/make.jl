
push!(LOAD_PATH, "..")

using Documenter
using TensorFSTs

makedocs(
    sitename = "TensorFSTs.jl",
    repo = Remotes.GitLab("gitlab.lisn.upsacaly.fr", "fast/fst", "TensorFSTs.jl"),
    pages = [
        "Home" => "index.md",
        "Usage" => "usage.md",
        "FST Operations" => "ops.md",
        "API" => "api.md"
    ],
    modules = [TensorFSTs]
)
