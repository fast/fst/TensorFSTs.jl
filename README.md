# TensorFST.jl

A Julia package for for efficient and differentiable (weighted) Finite State
Transducers.

Have a look at the [examples](examples) and the [documentation](https://hebergement.universite-paris-saclay.fr/fast-asr/tensorfsts.jl)
to get started.

## Authorship
This package is mainly supported by the [LISN](https://www.lisn.upsaclay.fr/)
but has benefitted from many external contribution (see the [Project.toml](Project.toml) file for an exhaustive list).

