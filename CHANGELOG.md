# Releases

## 2.0.0 -
### Changed
- do not model Finite-State Transducer but aribitrary `N`-order automaton.

## 1.4.1 - 01.03.2024
## Fixed
- `project_to` function was not exported

## 1.4.0 - 01.03.2024
## Added
- Limited support of automatic differentiation

## 1.3.0 - 29.02.2024
## Added
- Weightpushing algorithm

## 1.2.2 - 26.02.2024
## Fixed
- tokenfst: is accept only sequence of 1 token.

## 1.2.1 - 22.02.2024
## Fixed
- tokenfst: inconsitency between states and emissions
- compose_eps: inconsitency between states and emissions

## 1.2.0 - 20.02.2024
## Added
- composition with fst having epsilon arcs.
## Changed
- it is possible to specify the cardinality of the input / output symbol
  sets in the `TensorFST` constructor
## Improved
- renamed the internal association dimension - meaning for better code clarity:
  `(Sdim=..., Ddim=..., Idim=..., Odim=...)` to `(src=..., isym=..., osym=..., dest=...)`

## 1.1.0 - 13.02.2024
### Added
- New libraries: `NGramFSTs` and `SpeechRecognitionFSTs`

## 1.0.1 - 12.02.2024
### Fixed
- TensorFST accepts vector with no specified element type

## 1.0.0 - 08.02.2024
First version stable with:
- rational operation
- composition
- weight and shortestdistance

## 0.4.0 - 24.11.2023
## Changed
* Moved `sparsesemiodules` into a separate package.
* Simplification o the TensorFST interface.

## 0.3.0 - 20.09.2023
### Improvements
* Simplified hierarchy only FST type (no FSA)
### Added
* creation of FST from regular expression with the `compile`
  function. For instance `compile("a[1-9]b"; format = :regex)`
* FST are semiring element (with the `\otimes`, `\oplus` and `closure`
  functions)

## 0.2.0 - 15.09.2023
### Added
* Rational operations: union (`union`), concatenation (`cat`) and
  closure (`closure`).

## 0.1.1 - 14.09.2023
### Improvements
* cleaner interface for `vecmatmul!` interface without using the
  `transpose` opertation

## 0.1.0 - 13.09.2023
* initial release
* constructing and drawing FSTs
* shortestdistance
