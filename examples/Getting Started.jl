### A Pluto.jl notebook ###
# v0.19.40

using Markdown
using InteractiveUtils

# ╔═╡ 6cec7e32-ec19-11ee-2d1e-09c83e418190
begin
	using Pkg
	Pkg.activate("..")

	using Semirings
	using TensorFSTs
	using SparseSemiringAlgebra
	using LinearAlgebra
end

# ╔═╡ b40071ad-82ff-4c7a-8b3b-7acb37c968e6
T = TropicalSemiring{Float32}

# ╔═╡ 55512d4f-df21-466d-bbe0-62e984bed077
Base.union(Set([1, 2]), Set([3, 4]))

# ╔═╡ 1fdc16b4-f0ac-45f5-b7c5-2d904c3b4d00
latin = merge(Dict(1 => "ϵ"), Dict(i+1 => string(c) for (i, c) in enumerate('a':'z')))

# ╔═╡ b5f53c83-3b8b-4710-b250-2e55af7c13e9
latin_id = Dict(v => k for (k, v) in latin)

# ╔═╡ 6ff98465-a841-4b08-bfb7-d85d1c1cb3bb
# ╠═╡ disabled = true
#=╠═╡
a = ones(2, 3, 4)
  ╠═╡ =#

# ╔═╡ 194152bc-7af8-43da-86b6-951fbb7b9f38
A = SparseTensorFSM{T,2}(
	[
		Arc(1, 1, T(2), latin_id["a"], latin_id["p"]),
		Arc(1, 2, T(3), latin_id["b"], latin_id["q"]),
		Arc(2, 2, T(4), latin_id["c"], latin_id["r"])
	],
	[1 => one(T)],
	[2 => T(5)],
)

# ╔═╡ 62b14167-1cb4-4d4c-84eb-00e6a4a415f8
draw(A, latin) # equivalent to draw(A, latin, latin)

# ╔═╡ 725a9cdd-eca0-43c2-8078-b596161f2255
B = SparseTensorFSM{T,2}(
	[
		Arc(1, 2, T(0), ϵ, ϵ),
		Arc(2, 2, T(2), latin_id["a"], latin_id["p"]),
		Arc(2, 3, T(3), latin_id["b"], latin_id["q"]),
		Arc(3, 3, T(4), latin_id["c"], latin_id["r"]),
		Arc(3, 2, T(5), ϵ, ϵ)
	],
	[1 => one(T)],
	[3 => T(5)]
)

# ╔═╡ 40e36e71-639c-45bd-a29a-b8e7400f63f7
draw(B, latin)

# ╔═╡ 1a01c0c6-874e-4afc-acbb-aa83710a5141
md"""
## Rational Operations

### Union
"""

# ╔═╡ 761582af-796d-4e3f-aeb7-36f0c106ec5b
draw(fsmunion(A, B), latin)

# ╔═╡ 7a090e5c-b5d6-4b2f-a28d-5f6e74066309
md"""
### Concatenation
"""

# ╔═╡ f0e0411e-486a-42c9-b002-94352157d143
draw(A * B, latin)

# ╔═╡ 66187210-5b6f-454f-a3e8-b90515ba827d
md"### Closure"

# ╔═╡ aa5fd65c-6108-46b0-8839-db7ace9e9638
draw(A^*, latin)

# ╔═╡ d78b4e42-f9d4-4042-89a9-b5864b844759
draw(A^+, latin) # Closure plus

# ╔═╡ 913bf1db-02b8-4783-bb56-bc8c0edde2d7
md"""
## Elementary unary operation

### Projection
"""

# ╔═╡ 28ae4fcf-3c4f-4f4f-aec2-01455568fb96
draw(fsmproject(A; dims = 2), latin)

# ╔═╡ d5572c90-b5b7-49e4-8c24-5193b8f03196
draw(fsmproject(A; dims = 1), latin)

# ╔═╡ 8fc36715-a764-4f86-a848-c28a05179bd2
md"""
### Inversion
"""

# ╔═╡ d3a71d55-ce4a-42ea-aab0-80e76ad6802d
draw(fsmpermute(A, (2, 1)), latin)

# ╔═╡ 06a69aa8-493b-4fda-974b-4685105be711
md"""
### Reversal
"""

# ╔═╡ 1943822d-3512-4778-9042-680208b149ee
draw(fsmreverse(A), latin)

# ╔═╡ e2c851dc-ab98-49fa-853d-f9c3a2b1b8f2
notes = Dict(
	1 => "ϵ", 
	2 => "do", 
	3 => "ré", 
	4 => "mi", 
	5 => "fa", 
	6 => "sol", 
	7 => "la", 
	8 => "si"
)

# ╔═╡ 8443c116-ea00-4f18-919b-7411bf4351af
durations = Dict(
	1 => "ϵ", 
	2 => " 𝅝  ", 
	3 => " 𝅗𝅥  ",
	4 => " 𝅘𝅥  ",
	5 => " 𝅘𝅥𝅮  ",
	7 => " 𝅘𝅥𝅯  ",
	8 => " 𝅘𝅥𝅰  "
)

# ╔═╡ 84a8b06b-586d-4cb0-a5fe-71a3a926195b
melody = SparseTensorFSM{BoolSemiring,2}(
	[
		Arc(1, 2, one(BoolSemiring), 2, ϵ),
		Arc(1, 2, one(BoolSemiring), 4, ϵ),
		Arc(1, 2, one(BoolSemiring), 6, ϵ),
		Arc(2, 3, one(BoolSemiring), ϵ, 2),
		Arc(2, 3, one(BoolSemiring), ϵ, 3),
		Arc(2, 3, one(BoolSemiring), ϵ, 4)
	],
	[1 => one(BoolSemiring)],
	[3 => one(BoolSemiring)]
)

# ╔═╡ 267ac756-b36e-4421-9b60-233274d54e35
draw(melody, notes, durations)

# ╔═╡ 5ccf7bf4-260e-487b-bd14-77b0af31682c
md"""
## Composition
"""

# ╔═╡ b52b7305-4667-422f-a076-a30a2e4473a6
X = SparseTensorFSM{T,2}(
	[
		Arc(1, 2, T(0.1), latin_id["a"], latin_id["b"]),
		Arc(2, 1, T(0.2), latin_id["a"], latin_id["b"]),
		Arc(2, 3, T(0.3), latin_id["b"], latin_id["b"]),
		Arc(2, 4, T(0.4), latin_id["b"], latin_id["b"]),
		Arc(3, 4, T(0.5), latin_id["a"], latin_id["b"]),
		Arc(4, 4, T(0.6), latin_id["a"], latin_id["a"])
		
	],
	[1 => one(T)],
	[4 => T(0.7)]
)

# ╔═╡ 55340c65-dd00-4ae1-be6a-d479c4096113
Y = SparseTensorFSM{T,2}(
	[
		Arc(1, 2, T(0.1), latin_id["b"], latin_id["b"]),
		Arc(2, 2, T(0.2), latin_id["b"], latin_id["a"]),
		Arc(2, 3, T(0.3), latin_id["a"], latin_id["b"]),
		Arc(2, 4, T(0.4), latin_id["a"], latin_id["b"]),
		Arc(3, 4, T(0.5), latin_id["b"], latin_id["a"])
		
	],
	[1 => one(T)],
	[4 => T(0.6)]
)

# ╔═╡ b0982ceb-2a3d-451e-ab9b-a86500dba4dc
draw(Y, latin)

# ╔═╡ 213c16b2-7cf5-4359-906a-d2ff27d9609b
draw(X, latin)

# ╔═╡ 18e766f1-ebfd-4892-ac75-c8b69917c6c4
draw(fsmcompose(X, Y), latin)

# ╔═╡ 80f55f5d-4d0f-4904-8ac0-a1d0ce796f89
U = SparseTensorFSM{T,2}(
	[
		Arc(1, 2, one(T), latin_id["a"], latin_id["a"]),
		Arc(2, 3, one(T), latin_id["b"], ϵ),
		Arc(3, 4, one(T), latin_id["c"], ϵ),
		Arc(4, 5, one(T), latin_id["d"], latin_id["d"])
	],
	[1 => one(T)],
	[5 => one(T)]
)

# ╔═╡ 2756b6b9-ce77-4bbb-9231-f798312b5247
V = SparseTensorFSM{T,2}(
	[
		Arc(1, 2, T(0.1), latin_id["a"], latin_id["d"]),
		Arc(2, 3, T(0.2), ϵ, latin_id["e"]),
		Arc(3, 4, T(0.3), latin_id["d"], latin_id["a"]),
		
	],
	[1 => one(T)],
	[4 => one(T)]
)

# ╔═╡ dcdfbdf4-c0d0-452b-bb5f-2d8666635465
draw(U, latin)

# ╔═╡ 9db9f2bf-2961-4490-918d-fb217beee7d5
draw(V, latin)

# ╔═╡ 05524156-1cde-4863-ac1f-0312623be982


# ╔═╡ 59defcb2-25fb-4fdc-848d-149647394d57
draw(fsmcompose(U, V), latin)

# ╔═╡ 5a9f8ddb-627a-47fd-a34d-3fc1f74f5bc8
begin
	C = SparseTensorFSM{T,2}(
	[
		Arc(1, 2, T(0.1), latin_id["b"], latin_id["b"]),
		Arc(1, 2, T(0.3), latin_id["a"], latin_id["b"]),
		Arc(2, 3, T(0.3), latin_id["a"], latin_id["b"]),
		Arc(2, 4, T(0.4), latin_id["a"], latin_id["b"]),
		Arc(3, 4, T(0.5), latin_id["b"], latin_id["a"])		
	],
	[1 => one(T)],
	[4 => T(0.6)]
)
	draw(C)
end

# ╔═╡ 2de01a2c-02bf-4e6a-aff3-a2369a24e5bf
fsmshortestdistance(C)

# ╔═╡ Cell order:
# ╠═6cec7e32-ec19-11ee-2d1e-09c83e418190
# ╠═b40071ad-82ff-4c7a-8b3b-7acb37c968e6
# ╠═55512d4f-df21-466d-bbe0-62e984bed077
# ╠═1fdc16b4-f0ac-45f5-b7c5-2d904c3b4d00
# ╠═b5f53c83-3b8b-4710-b250-2e55af7c13e9
# ╠═6ff98465-a841-4b08-bfb7-d85d1c1cb3bb
# ╠═194152bc-7af8-43da-86b6-951fbb7b9f38
# ╠═62b14167-1cb4-4d4c-84eb-00e6a4a415f8
# ╠═725a9cdd-eca0-43c2-8078-b596161f2255
# ╠═40e36e71-639c-45bd-a29a-b8e7400f63f7
# ╟─1a01c0c6-874e-4afc-acbb-aa83710a5141
# ╠═761582af-796d-4e3f-aeb7-36f0c106ec5b
# ╟─7a090e5c-b5d6-4b2f-a28d-5f6e74066309
# ╠═f0e0411e-486a-42c9-b002-94352157d143
# ╟─66187210-5b6f-454f-a3e8-b90515ba827d
# ╠═aa5fd65c-6108-46b0-8839-db7ace9e9638
# ╠═d78b4e42-f9d4-4042-89a9-b5864b844759
# ╟─913bf1db-02b8-4783-bb56-bc8c0edde2d7
# ╠═28ae4fcf-3c4f-4f4f-aec2-01455568fb96
# ╠═d5572c90-b5b7-49e4-8c24-5193b8f03196
# ╟─8fc36715-a764-4f86-a848-c28a05179bd2
# ╠═d3a71d55-ce4a-42ea-aab0-80e76ad6802d
# ╟─06a69aa8-493b-4fda-974b-4685105be711
# ╠═1943822d-3512-4778-9042-680208b149ee
# ╠═e2c851dc-ab98-49fa-853d-f9c3a2b1b8f2
# ╠═8443c116-ea00-4f18-919b-7411bf4351af
# ╠═84a8b06b-586d-4cb0-a5fe-71a3a926195b
# ╠═267ac756-b36e-4421-9b60-233274d54e35
# ╟─5ccf7bf4-260e-487b-bd14-77b0af31682c
# ╠═b52b7305-4667-422f-a076-a30a2e4473a6
# ╠═55340c65-dd00-4ae1-be6a-d479c4096113
# ╠═b0982ceb-2a3d-451e-ab9b-a86500dba4dc
# ╠═213c16b2-7cf5-4359-906a-d2ff27d9609b
# ╠═18e766f1-ebfd-4892-ac75-c8b69917c6c4
# ╠═80f55f5d-4d0f-4904-8ac0-a1d0ce796f89
# ╠═2756b6b9-ce77-4bbb-9231-f798312b5247
# ╠═dcdfbdf4-c0d0-452b-bb5f-2d8666635465
# ╠═9db9f2bf-2961-4490-918d-fb217beee7d5
# ╠═05524156-1cde-4863-ac1f-0312623be982
# ╠═59defcb2-25fb-4fdc-848d-149647394d57
# ╠═5a9f8ddb-627a-47fd-a34d-3fc1f74f5bc8
# ╠═2de01a2c-02bf-4e6a-aff3-a2369a24e5bf
